class ChannelType < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  mount_uploader :image, ImageUploader
  has_many :channels
  with_options presence: true do |channel_type|
    channel_type.validates :title, uniqueness: true
    channel_type.validates :description
  end
end
