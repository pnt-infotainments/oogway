class ChannelImage < ActiveRecord::Base
  belongs_to :channel
  mount_uploader :image, ImageUploader
end
