class Member < ActiveRecord::Base
  belongs_to :user
  belongs_to :klass
  # validate :owner_check
  validates :end_date, presence: true, on: :update
  validates :remarks, presence: true, on: :update, if: :quit_remarks

  def quit_remarks
  	block || !status 
  end

  def owner_check
  	if user_id == klass.user.id
  		errors.add(:status, "Can't follow your own class")
  	end
  end
end
