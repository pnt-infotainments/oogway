class Answer < ActiveRecord::Base
  belongs_to :channel
  belongs_to :user
  belongs_to :post
  belongs_to :question
end
