class Klass < ActiveRecord::Base
  belongs_to :user
  extend FriendlyId
  friendly_id :title, use: :slugged
  validates :title, presence: true, uniqueness: true
  validates :description, presence: true
  validates :code, presence: true, uniqueness: true
  has_many :members, dependent: :destroy
  has_many :students, through: :members, source: :user, foreign_key: :user_id
  has_many :posts, as: :postable, dependent: :destroy
  has_many :assignments, as: :assignable
  mount_uploader :avatar, ImageUploader

  def fetch_title
  	title
  end
  
  def subscriptions
    members
  end
end
