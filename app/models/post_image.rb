class PostImage < ActiveRecord::Base
  belongs_to :post_content
  belongs_to :post
  belongs_to :channel
  mount_uploader :image, ImageUploader
end
