class Status < ActiveRecord::Base
	has_many :posts
	has_many :channels
	has_many :post_categories
	with_options presence: true do |status|
    status.validates :name, uniqueness: true
    status.validates :description
  end
end
