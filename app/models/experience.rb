class Experience < ActiveRecord::Base
  belongs_to :profile
  belongs_to :user
  with_options presence: true do |exp|
  	exp.validates :company_name
  	exp.validates :designation
  	exp.validates :from
  	exp.validates :to
  end
end
