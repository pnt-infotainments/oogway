class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, 
         :validatable, :omniauthable

  TEMP_EMAIL_PREFIX = 'change@me'
  TEMP_EMAIL_REGEX = /\Achange@me/

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable
  # belongs_to :union
  validates :user_name, presence: true, uniqueness: true
  validate :validate_user_roles 
  acts_as_voter
  has_many :activities, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :channels, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :authorizations, dependent: :destroy
  has_many :my_subscriptions, 
            -> { where("subscriptions.subscribable_type" => 'Channel',
                       "subscriptions.subscribe" => true) },
            class_name: Subscription.name,
  					foreign_key: :user_id
 	has_many :followers,
            -> { where("subscriptions.subscribe" => true ) },
            as: :subscribable,
            class_name: Subscription.name
 	has_many :following, 
 						-> { where("subscriptions.subscribable_type" => 'User',
                       "subscriptions.subscribe" => true ) },
 						class_name: Subscription.name, 
 						foreign_key: :user_id
  has_one :profile, dependent: :destroy
  has_many :received_mails, dependent: :destroy,
            class_name: Message.name,
            foreign_key: :recipient_id
  has_many :sent_mails, dependent: :destroy,
            class_name: Message.name,
            foreign_key: :sender_id
  has_many :user_roles, dependent: :destroy
  has_many :roles, through: :user_roles, dependent: :destroy
  has_many :user_interests, dependent: :destroy
  has_many :interests, dependent: :destroy
  has_many :klasses, dependent: :destroy
  has_many :members, dependent: :destroy
  has_many :my_classes, through: :members, source: :klass, foreign_key: :klass_id
  # validates_format_of :email, without: TEMP_EMAIL_REGEX, on: :update

  # def self.from_omniauth(auth, current_user)
  #   authorization = Authorization.where(:provider => auth.provider, :uid => auth.uid.to_s, :token => auth.credentials.token, :secret => auth.credentials.secret).first_or_initialize
  #   if authorization.user.blank?
  #     user = current_user || User.where('email = ?', auth["info"]["email"]).first
  #     if user.blank?
  #      user = User.new
  #      user.password = Devise.friendly_token[0,10]
  #      user.name = auth.info.name
  #      user.email = auth.info.email
  #      if auth.provider == "twitter" 
  #        user.save(:validate => false) 
  #      else
  #        user.save
  #      end
  #    end
  #    authorization.username = auth.info.nickname
  #    authorization.user_id = user.id
  #    authorization.save
  #  end
  #  authorization.user
  # end 

  def self.find_for_oauth(auth, signed_in_resource = nil)

    # Get the identity and user if they exist
    identity = Authorization.find_for_oauth(auth)

    # If a signed_in_resource is provided it always overrides the existing user
    # to prevent the identity being locked with accidentally created accounts.
    # Note that this may leave zombie accounts (with no associated identity) which
    # can be cleaned up at a later date.
    user = signed_in_resource ? signed_in_resource : identity.user

    # Create the user if needed
    if user.nil?
      puts auth.inspect
      # Get the existing user by email if the provider gives us a verified email.
      # If no verified email was provided we assign a temporary email and ask the
      # user to verify it on the next step via UsersController.finish_signup
      email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
      email = auth.info.email
      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if user.nil?
        user = User.new(
          # name: auth.extra.raw_info.name,
          #username: auth.info.nickname || auth.uid,
          email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
          password: Devise.friendly_token[0,20]
        )
        # user.skip_confirmation!
        user.save!
      end
    end

     # Associate the identity with the user if needed
    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end

  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end

  def subscribed?(channel_id)
    my_subscriptions.find_by_subscribable_id(channel_id).try(:subscribe)
  end

  def member?(klass_id)
    members.find_by_klass_id(klass_id).try(:status)
  end

  def follow?(user_id)
    following.find_by_subscribable_id(user_id).try(:subscribe)
  end

  def has_role?(admin)
    user_roles = roles.pluck(:name).uniq
    user_roles && user_roles.include?(admin)
  end

  def validate_user_roles
    role = Role.find_by_id(role_ids) if role_ids.present?
    if role_ids.blank?
      errors.add("role_ids", "can't be blank!")
    elsif role.try(:name) == 'Administrator'
      errors.add("role_ids", "please choose role")
    elsif role.blank?
      errors.add("role_ids", "please choose role")
    end
  end
end
