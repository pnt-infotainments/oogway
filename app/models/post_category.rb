class PostCategory < ActiveRecord::Base
	extend FriendlyId
  friendly_id :name, use: :slugged
	belongs_to :channel
	has_many :posts
	belongs_to :union
end
