class Achievement < ActiveRecord::Base
  belongs_to :profile
  belongs_to :user
  with_options presence: true do |ach|
  	ach.validates :title
  	ach.validates :description
  end
end
