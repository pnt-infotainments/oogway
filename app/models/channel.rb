class Channel < ActiveRecord::Base
	extend FriendlyId
	mount_uploader :image, ImageUploader
  friendly_id :name, use: :slugged
	has_many :post_categories
	has_many :posts, as: :postable, dependent: :destroy
	has_many :post_contents
	has_many :post_images
	# has_many :channel_images
	has_many :subscriptions,
            -> { where("subscriptions.subscribe" => true ) },
            as: :subscribable,
            class_name: Subscription.name
	has_many :questions
	belongs_to :union
	belongs_to :user
	belongs_to :status
	belongs_to :channel_type
	has_many :assignments, as: :assignable
	with_options presence: true do |channel|
    channel.validates :name, uniqueness: true
    channel.validates :description
    channel.validates :channel_type_id
  end
  # Scope
  scope :active, -> {where(status: Status.find_by_name('Accepted'))}

  def fetch_title
  	name
  end

  def tags
  	Tag.joins(:taggings).where('taggings.taggable_id' => 
                              self.posts.pluck(:id),
                              'taggings.taggable_type' => Post.name)
  end

  def contributors
  	User.joins(:posts
  						).select('users.*, count(posts.*) as post_count'
                      ).where("users.id" => self.posts.pluck(:user_id), 
                              "posts.postable_type" => Channel.name,
                              "posts.postable_id" => id
                             ).group('users.id').order('post_count DESC')
	end

end
