class Post < ActiveRecord::Base
	extend FriendlyId
  friendly_id :name, use: :slugged
	belongs_to :post_category
	belongs_to :user
	belongs_to :status
  belongs_to :postable, polymorphic: true
	acts_as_votable
	has_many :votes, as: :votable
	has_many :embed_codes, dependent: :destroy
	has_many :post_images, dependent: :destroy
	has_many :post_contents, dependent: :destroy
	has_many :subscriptions, as: :subscribable, dependent: :destroy
	has_many :comments, as: :commentable, dependent: :destroy
	has_many :taggings, as: :taggable, dependent: :destroy
	has_many :tags, through: :taggings
  has_many :attachments, as: :attachable, dependent: :destroy
	accepts_nested_attributes_for :post_images, allow_destroy: true, reject_if: :all_blank
	accepts_nested_attributes_for :post_contents, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :attachments, allow_destroy: true, reject_if: :all_blank
	with_options presence: true do |post|
    post.validates :name, uniqueness: true
    post.validates :description
  end

  scope :active, -> {where(status: Status.find_by_name('Accepted'))}

  def self.tagged_with(name)
  	Tag.find_by_name(name).try(:posts)
  end

  def tag_list
  	tags.map(&:name)
  end

  def tag_list=(names)
    names = [names] unless names.is_a? Array
  	names = names - ['', nil, 0]
  	self.tags = names.compact.map do |name|
  		if name.to_i.zero?
        Tag.find_or_create_by(name: name.squish.downcase)
      else
        Tag.find_by(id: name.strip.to_i)
      end
  	end
  end

end
