class Attachment < ActiveRecord::Base
  belongs_to :user
  belongs_to :attachable, polymorphic: true
  mount_uploader :link, DocumentUploader
  validates :link, presence: true
  validates :user_id, presence: true
end
