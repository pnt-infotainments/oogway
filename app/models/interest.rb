class Interest < ActiveRecord::Base
	has_many :user_interests, dependent: :destroy
	has_many :profiles, through: :user_interests, 
					  source: :profile_id
	has_many :users, through: :user_interests, 
					  source: :user_id
	validates :name, presence: true, uniqueness: true
end
