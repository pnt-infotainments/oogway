class Union < ActiveRecord::Base
	has_many :channels
	has_many :post_categories
	has_many :posts
	# has_many :users
end
