class UserInterest < ActiveRecord::Base
  belongs_to :user
  belongs_to :interest
  belongs_to :profile
end
