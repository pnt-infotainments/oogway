class Message < ActiveRecord::Base
	belongs_to :receiver, class_name: 'User', foreign_key: :recipient_id
	belongs_to :sender, class_name: 'User', foreign_key: :sender_id
	with_options presence: true do |message|
    message.validates :recipient_id
    message.validates :sender_id
    message.validates :subject
    message.validates :body
  end
end
