class Subscription < ActiveRecord::Base
  belongs_to :user
  belongs_to :subscribable, polymorphic: true
  scope :subscribed, ->{where(subscribe: true)}
end
