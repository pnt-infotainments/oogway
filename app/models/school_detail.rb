class SchoolDetail < ActiveRecord::Base
  belongs_to :user
  belongs_to :profile
  with_options presence: true do |sd|
  	sd.validates :std
  	sd.validates :name
  end
end
