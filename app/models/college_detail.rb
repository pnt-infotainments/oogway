class CollegeDetail < ActiveRecord::Base
  belongs_to :college
  belongs_to :profile
  belongs_to :user
  with_options presence: true do |cd|
  	cd.validates :degree
  	cd.validates :year
  	cd.validates :college_id
  	cd.validates :department
  end
end
