class Assignment < ActiveRecord::Base
  belongs_to :assignable, polymorphic: true
  belongs_to :user
  has_many :attachments, as: :attachable, dependent: :destroy
  belongs_to :klass, foreign_key: :assignable_id, foreign_type: 'Klass'
  validates :title, presence: true
  validates :description, presence: true
  validate :validate_due_date

  def validate_due_date
  	if due_date.blank?
  		errors.add(:due_date, "Can't be blank")
  	elsif Date.parse(due_date.to_s) <= Date.today
  		errors.add(:due_date, "Should be greater than current date")
  	end
  end

end
