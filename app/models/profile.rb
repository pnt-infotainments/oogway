class Profile < ActiveRecord::Base
  belongs_to :user
  has_many :college_details, dependent: :destroy
  has_many :school_details, dependent: :destroy
  has_many :achievements, dependent: :destroy
  has_many :experiences, dependent: :destroy
  has_many :projects, dependent: :destroy
  has_many :user_interests, dependent: :destroy
  has_many :interests, through: :user_interests, dependent: :destroy
  has_many :skill_sets, dependent: :destroy, as: :skillable
  has_many :skills, through: :skill_sets, dependent: :destroy
  mount_uploader :avatar, ImageUploader
  mount_uploader :cover_photo, ImageUploader
  with_options presence: true do |profile|
    profile.validates :first_name
    profile.validates :last_name
    profile.validates :user_id
  end

	def full_name
		"#{first_name} #{last_name}"
	end

  def interested_in
    interests.map(&:name)
  end

  def interested_in_list
    interests.map(&:name).join(', ')
  end

  def interested_in=(names)
    names = [names] unless names.is_a? Array
    names = names - ['', nil, 0]
    self.interests = names.map do |n|
      if n.to_i.zero?
        Interest.find_or_create_by(name: n.strip)
      else
        Interest.find_by_id(n.to_i)
      end
    end
  end

  def skill_list
    skills.map(&:name)
  end

  def skills_list
    skills.map(&:name).join(', ')
  end

  def skill_list=(names)
    names = [names] unless names.is_a? Array
    names = names - ['', nil, 0]
    self.skills = names.map do |n|
      if n.to_i.zero?
        Skill.find_or_create_by(name: n.strip)
      else
        Skill.find_by_id(n.to_i)
      end
    end
  end


end
