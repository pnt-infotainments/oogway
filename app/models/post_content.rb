class PostContent < ActiveRecord::Base
  belongs_to :post
  belongs_to :channel
  has_many :content_images
  accepts_nested_attributes_for :content_images, allow_destroy: true, reject_if: :all_blank
  validates_presence_of :name
  validates_presence_of :description

end
