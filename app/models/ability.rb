class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities

    user ||= User.new # guest user
    
    # if user.has_role? :administrator
    #   can :manage, :all
    # end
    can :read, :all
    can :create, Comment
    can :manage, Comment do |comment|
      comment.try(:user) == user
    end
    # if user.role?(:author)
    # end
    can :create, Post
    can :manage, Post do |post|
      post.try(:user) == user
    end

    can :create, Message
    can :manage, Message do |message|
      (message.try(:sender_id) == user.id || \
      message.try(:receiver_id) == user.id) 
    end

    can :create, Channel
    can :manage, Channel do |channel|
      channel.try(:user) == user
    end
    
    can :create, Profile
    can :manage, Profile do |profile|
      profile.try(:user) == user
    end

    # if user.has_role?('Mentor')
      can :create, Klass
      can :manage, Klass do |klass|
        klass.user == user
      end
    # end

    can :create, Assignment
    can :manage, Assignment do |assignment|
      assignment.user == user
    end

  end
end
