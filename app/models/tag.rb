class Tag < ActiveRecord::Base
	has_many :taggings
	has_many :posts, through: :taggings, 
					 source: :taggable, source_type: 'Post'
	validates :name,  presence: true, uniqueness: true

	def self.list
		Tag.includes(:taggings).all.order(:name)
	end

	def self.popular_post(name = nil, char = nil)
		name_query = {"tags.name" => name } if name
		Tag.joins(:taggings
						 ).select("tags.*, 
											count(taggings.*) AS tags_count"
										 ).where("taggings.taggable_type" => Post.name
										 ).where(name_query).group('tags.id').order('tags_count DESC')
	end
end
