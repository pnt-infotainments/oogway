class Project < ActiveRecord::Base
  belongs_to :user
  belongs_to :profile
  has_many :skill_sets, as: :skillable, dependent: :destroy
  has_many :skills, through: :skill_sets, dependent: :destroy
  with_options presence: true do |project|
    project.validates :title
    project.validates :description
    project.validates :type
    project.validates :from
    project.validates :to
    project.validates :role
    project.validates :team_size
    project.validates :skill_list
  end

  def skill_list
    skills.map(&:name).join(", ")
  end

  def skill_list=(names)
    names = [names] unless names.is_a? Array
    names = names - ['', nil, 0]
    self.skills = names.map do |n|
      if n.to_i.zero?
        Skill.find_or_create_by(name: n.strip)
      else
        Skill.find_by_id(n.to_i)
      end
    end
  end

end
