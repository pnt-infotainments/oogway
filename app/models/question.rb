class Question < ActiveRecord::Base
  belongs_to :channel
  belongs_to :user
  belongs_to :post
  has_many :answers
end
