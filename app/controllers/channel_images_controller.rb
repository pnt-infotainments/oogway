class ChannelImagesController < ApplicationController
  before_action :set_channel_image, only: [:show, :edit, :update, :destroy]

  # GET /channel_images
  # GET /channel_images.json
  def index
    @channel_images = ChannelImage.all
  end

  # GET /channel_images/1
  # GET /channel_images/1.json
  def show
  end

  # GET /channel_images/new
  def new
    @channel_image = ChannelImage.new
  end

  # GET /channel_images/1/edit
  def edit
  end

  # POST /channel_images
  # POST /channel_images.json
  def create
    @channel_image = ChannelImage.new(channel_image_params)

    respond_to do |format|
      if @channel_image.save
        format.html { redirect_to @channel_image, notice: 'Post type image was successfully created.' }
        format.json { render :show, status: :created, location: @channel_image }
      else
        format.html { render :new }
        format.json { render json: @channel_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /channel_images/1
  # PATCH/PUT /channel_images/1.json
  def update
    respond_to do |format|
      if @channel_image.update(channel_image_params)
        format.html { redirect_to @channel_image, notice: 'Post type image was successfully updated.' }
        format.json { render :show, status: :ok, location: @channel_image }
      else
        format.html { render :edit }
        format.json { render json: @channel_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /channel_images/1
  # DELETE /channel_images/1.json
  def destroy
    @channel_image.destroy
    respond_to do |format|
      format.html { redirect_to channel_images_url, notice: 'Post type image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_channel_image
      @channel_image = ChannelImage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def channel_image_params
      params.require(:channel_image).permit(:name, :description, :channel_id)
    end
end
