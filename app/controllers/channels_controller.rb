class ChannelsController < ApplicationController
  before_action :authenticate_user!, except: [:index, 
                                              :show,
                                              :explore]
  before_action :find_channel, only: [:show, :edit, :followers,
                                      :update, :destroy,
                                      :subscribe, :unfollow]
  def index
    channel_type = ChannelType.find_by_slug(params[:type])
    @channels = if channel_type
                  channel_type.channels.page(params[:page]).per(10)
                else
                  Channel.page(params[:page]).per(5)
                end
  end
  
  def show
    @posts = @channel.posts.page(params[:page]).per(5)
    @tags = @channel.try(:tags)
    @contributors = @channel.try(:contributors)
  end

  def new
    @channel = Channel.new
  end

  def latest_channels
    @latest_channels = Channel.where('(created_at)::date = (now):date')
  end

  def my
    @channels = current_user.channels
  end
  
  def recommended
    @recommended = Channel.joins(:channel_type, :subscriptions
                                ).select('channels.*, channel_types.title as category, subscriptions.*' 
                                ).where('subscriptions.subscribe' => [nil, false]
                                ).where('subscriptions.user_id != ?', current_user)
  end

  # GET /channels/1/edit
  def edit
    authorize! :edit, @channel
  end

  # POST /channels
  # POST /channels.json
  def create
    @channel = Channel.new(channel_params)
    @channel.user = current_user
    @channel.status = Status.find_by_name('Submitted')

    respond_to do |format|
      if @channel.save
        track_activity(@channel)
        format.html { redirect_to channel_path(@channel), notice: 'Post type was successfully created.' }
        format.json { render :show, status: :created, location: @channel }
      else
        format.html { render :new }
        format.json { render json: @channel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /channels/1
  # PATCH/PUT /channels/1.json
  def update
    authorize! :update, @channel
    respond_to do |format|
      if @channel.update(channel_params)
        track_activity(@channel)
        format.html { redirect_to channel_path(@channel), notice: 'Post type was successfully updated.' }
        format.json { render :show, status: :ok, location: @channel }
      else
        format.html { render :edit }
        format.json { render json: @channel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /channels/1
  # DELETE /channels/1.json
  def destroy
    authorize! :destroy, @channel
    @channel.destroy
    track_activity(@channel)
    respond_to do |format|
      format.html { redirect_to channels_path, notice: 'Post type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def subscribe
  	SubscribeService.new({channel: @channel,
                          current_user: current_user 
                          }).subscribe
  end

  def unfollow
    SubscribeService.new({channel: @channel,
                          current_user: current_user 
                          }).unsubscribe
  end

  def list_channels
    channel_ids = current_user.my_subscriptions.map(&:subscribable_id)
    channel_ids << current_user.channels.pluck(&:id)
    @channels = Channel.where(id: channel_ids.flatten)
    render layout: false
  end

  def explore
    @channel_types = ChannelType.includes(channels: :subscriptions).all
    most_followed = ChannelType.joins(channels: :subscriptions
                                     ).select('channel_types.*, count(subscriptions.*) 
                                               as subscribed_count'
                                             ).group('channel_types.id'
                                                    ).order('subscribed_count DESC').first
    if most_followed
      most_followed.title = 'Most Popular' 
      @channel_types << most_followed
    end
    trending = ChannelType.joins(channels: :posts
                                ).select('channel_types.*,
                                         count(posts.*) 
                                         as posts_count'
                                         ).group('channel_types.id'
                                                ).order('posts_count DESC').first
    if trending
      trending.title = 'Trending'
      @channel_types << trending
    end
  end

  def browse
    @channels = Channel.all
  end

  def followers
    @followers = User.where(id: @channel.subscriptions.pluck(:user_id))
  end

  private
  
  def find_channel
  	@channel = Channel.find_by_slug(params[:id])
  end

  def channel_params
      params.require(:channel
                    ).permit(:name, :description, :status_id,
                             :channel_type_id, :image, :image_cache)
  end

end
