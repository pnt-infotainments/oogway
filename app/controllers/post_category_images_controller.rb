class PostCategoryImagesController < ApplicationController
  before_action :set_post_category_image, only: [:show, :edit, :update, :destroy]

  # GET /post_category_images
  # GET /post_category_images.json
  def index
    @post_category_images = PostCategoryImage.all
  end

  # GET /post_category_images/1
  # GET /post_category_images/1.json
  def show
  end

  # GET /post_category_images/new
  def new
    @post_category_image = PostCategoryImage.new
  end

  # GET /post_category_images/1/edit
  def edit
  end

  # POST /post_category_images
  # POST /post_category_images.json
  def create
    @post_category_image = PostCategoryImage.new(post_category_image_params)

    respond_to do |format|
      if @post_category_image.save
        format.html { redirect_to @post_category_image, notice: 'Post category image was successfully created.' }
        format.json { render :show, status: :created, location: @post_category_image }
      else
        format.html { render :new }
        format.json { render json: @post_category_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /post_category_images/1
  # PATCH/PUT /post_category_images/1.json
  def update
    respond_to do |format|
      if @post_category_image.update(post_category_image_params)
        format.html { redirect_to @post_category_image, notice: 'Post category image was successfully updated.' }
        format.json { render :show, status: :ok, location: @post_category_image }
      else
        format.html { render :edit }
        format.json { render json: @post_category_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /post_category_images/1
  # DELETE /post_category_images/1.json
  def destroy
    @post_category_image.destroy
    respond_to do |format|
      format.html { redirect_to post_category_images_url, notice: 'Post category image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post_category_image
      @post_category_image = PostCategoryImage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_category_image_params
      params.require(:post_category_image).permit(:name, :description, :post_category_id)
    end
end
