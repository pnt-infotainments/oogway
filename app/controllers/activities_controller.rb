class ActivitiesController < ApplicationController
	def index
		@activities = current_user.activities.includes(:trackable).order('created_at DESC').all
	end
end
