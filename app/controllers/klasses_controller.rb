class KlassesController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update,
                                            :destroy, :join]
  before_action :set_klass, only: [:show, :edit, 
                                   :update, :destroy,
                                   :join]
  # GET /klasses
  # GET /klasses.json
  def index
    @klasses = Klass.includes(user: :profile).all
  end

  def explore
    @classes = Klass.includes(:members, user: :profile).all
  end

  def join
    status = MemberService.new(current_user, @klass).join_class
    if status
      flash[:notice] = 'Joined class successfully'
    else
      flash[:error] = 'Unable to join class'
    end
    redirect_to action: :explore
  end
  # GET /klasses/1
  # GET /klasses/1.json
  def show
  end

  # GET /klasses/new
  def new
    @klass = Klass.new
  end

  # GET /klasses/1/edit
  def edit
    authorize! :edit, @klass
  end

  # POST /klasses
  # POST /klasses.json
  def create
    @klass = Klass.new(klass_params)
    @klass.user = current_user
    respond_to do |format|
      if @klass.save
        format.html { redirect_to @klass, notice: 'Klass was successfully created.' }
        format.json { render :show, status: :created, location: @klass }
      else
        format.html { render :new }
        format.json { render json: @klass.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /klasses/1
  # PATCH/PUT /klasses/1.json
  def update
    authorize! :update, @klass
    respond_to do |format|
      if @klass.update(klass_params)
        format.html { redirect_to @klass, notice: 'Klass was successfully updated.' }
        format.json { render :show, status: :ok, location: @klass }
      else
        format.html { render :edit }
        format.json { render json: @klass.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /klasses/1
  # DELETE /klasses/1.json
  def destroy
    authorize! :destroy, @klass
    @klass.destroy
    respond_to do |format|
      format.html { redirect_to klasses_url, notice: 'Klass was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_klass
      @klass = Klass.find_by_slug(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def klass_params
      params.each do |key, value|
        next unless value.is_a? String
        params[key] = value.squish
      end
      params.require(:klass).permit(:title, :description, :code, :user_id, :avatar)
    end
end
