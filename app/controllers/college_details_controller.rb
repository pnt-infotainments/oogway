class CollegeDetailsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_college_detail, only: [:show, :edit, :update, :destroy]

  # GET /college_details
  # GET /college_details.json
  def index
    @college_details = CollegeDetail.all
  end

  # GET /college_details/1
  # GET /college_details/1.json
  def show
  end

  # GET /college_details/new
  def new
    @profile = current_user.profile
    @college_detail = @profile.college_details.new
  end

  # GET /college_details/1/edit
  def edit
  end

  # POST /college_details
  # POST /college_details.json
  def create
    @profile = current_user.profile
    @college_detail = @profile.college_details.new(college_detail_params)
    @college_detail.user = current_user
    respond_to do |format|
      if @college_detail.save
        track_activity(@college_detail, 'college detail')
        format.html { redirect_to college_details_url, notice: 'College detail was successfully created.' }
        format.json { render :show, status: :created, location: @college_detail }
      else
        format.html { render :new }
        format.json { render json: @college_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /college_details/1
  # PATCH/PUT /college_details/1.json
  def update
    respond_to do |format|
      if @college_detail.update(college_detail_params)
        format.html { redirect_to college_details_url, notice: 'College detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @college_detail }
      else
        format.html { render :edit }
        format.json { render json: @college_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /college_details/1
  # DELETE /college_details/1.json
  def destroy
    @college_detail.destroy
    respond_to do |format|
      format.html { redirect_to college_details_url, notice: 'College detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_college_detail
      @profile = current_user.profile
      @college_detail = @profile.college_details.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def college_detail_params
      params.require(:college_detail).permit(:degree, :college_id, :year, :department, :profile_id, :user_id)
    end
end
