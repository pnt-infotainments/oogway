class MessagesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_message, only: [:show, :edit, :update, :destroy]

  # GET /messages
  # GET /messages.json
  def index
    @messages = if params[:sent]
                  current_user.sent_mails.page(params[:page]).per(2)
                else
                  current_user.received_mails.page(params[:page]).per(2)
                end
  end

  # GET /messages/1
  # GET /messages/1.json
  def show
    @message =  if params[:sent]
                  current_user.sent_mails.find_by_id(params[:id])
                else
                  current_user.received_mails.find_by_id(params[:id])
                end
  end

  # GET /messages/new
  def new
    @message = Message.new(sender_id: current_user)
  end

  # GET /messages/1/edit
  def edit
    authorize! :edit, @message
  end

  # POST /messages
  # POST /messages.json
  def create
    @message = Message.new(message_params)
    @message.recipient_id = 
      User.find_by_user_name(params[:message][:recipient_id]).try(:id)
    @message.sender_id = current_user.id
    respond_to do |format|
      if @message.recipient_id.to_i.present? && @message.save 
        format.html { redirect_to messages_path, notice: 'Your mail is send.' }
        format.json { render :show, status: :created, location: @message }
      else
        format.html { render :new }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /messages/1
  # PATCH/PUT /messages/1.json
  def update
    authorize! :update, @message
    respond_to do |format|
      if @message.update(message_params)
        format.html { redirect_to @message, notice: 'Message was successfully updated.' }
        format.json { render :show, status: :ok, location: @message }
      else
        format.html { render :edit }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    authorize! :destroy, @message
    @message.destroy
    respond_to do |format|
      format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.where('id = ? AND sender_id = ?
                                OR recipient_id = ?', 
                                params[:id], current_user, 
                                current_user).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:recipient_id, :sender_id, :body, :subject)
    end
end
