class SchoolDetailsController < ApplicationController
  before_action :authenticate_user!, except: :index
  before_action :set_school_detail, only: [:show, :edit, :update, :destroy]

  # GET /school_details
  # GET /school_details.json
  def index
    @school_details = SchoolDetail.all
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /school_details/1
  # GET /school_details/1.json
  def show
  end

  # GET /school_details/new
  def new
    @profile = current_user.profile
    unless @profile
      flash[:warning] = "Please complete your profile"  
      return redirect_to basic_info_profile_path(user_name: current_user.user_name)
    end
    
    @school_detail = @profile.school_details.new
  end

  # GET /school_details/1/edit
  def edit
  end

  # POST /school_details
  # POST /school_details.json
  def create
    @profile = current_user.profile
    @school_detail = @profile.school_details.new(school_detail_params)
    @school_detail.user = current_user

    respond_to do |format|
      if @school_detail.save
        track_activity(@school_detail, 'school detail')
        format.html { redirect_to profile_path(user_name: current_user.user_name), notice: 'School detail was successfully created.' }
        format.json { render :show, status: :created, location: @school_detail }
      else
        format.html { render :new }
        format.json { render json: @school_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /school_details/1
  # PATCH/PUT /school_details/1.json
  def update
    respond_to do |format|
      if @school_detail.update(school_detail_params)
        format.html { redirect_to profile_school_details_url, notice: 'School detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @school_detail }
      else
        format.html { render :edit }
        format.json { render json: @school_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /school_details/1
  # DELETE /school_details/1.json
  def destroy
    @school_detail.destroy
    respond_to do |format|
      format.html { redirect_to profile_school_details_url(@profile), notice: 'School detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_school_detail
      @profile = current_user.profile
      @school_detail = @profile.school_details.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def school_detail_params
      params.require(:school_detail).permit(:user_id, :profile_id, :std, :name)
    end
end
