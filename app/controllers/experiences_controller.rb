class ExperiencesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_experience, only: [:show, :edit, :update, :destroy]

  # GET /experiences
  # GET /experiences.json
  def index
    @profile = current_user.profile
    redirect_to(basic_info_profile_path(user_name: current_user),
                notice: 'Please create profile first'
               ) unless @profile
    @experiences = @profile.try(:experiences)
  end

  # GET /experiences/1
  # GET /experiences/1.json
  def show
  end

  # GET /experiences/new
  def new
    @profile = current_user.profile
    @experience = @profile.experiences.new
  end

  # GET /experiences/1/edit
  def edit
    @profile = current_user.profile
  end

  # POST /experiences
  # POST /experiences.json
  def create
    @profile = current_user.profile
    @experience = @profile.experiences.new(experience_params)
    @experience.user = current_user
    respond_to do |format|
      if @experience.save
        track_activity(@experience, 'experience')
        format.html { redirect_to experiences_url, notice: 'Experience was successfully created.' }
        format.json { render :show, status: :created, location: @experience }
      else
        format.html { render :new }
        format.json { render json: @experience.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /experiences/1
  # PATCH/PUT /experiences/1.json
  def update
    respond_to do |format|
      if @experience.update(experience_params)
        format.html { redirect_to experiences_path, notice: 'Experience was successfully updated.' }
        format.json { render :show, status: :ok, location: @experience }
      else
        format.html { render :edit }
        format.json { render json: @experience.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /experiences/1
  # DELETE /experiences/1.json
  def destroy
    @experience.destroy
    respond_to do |format|
      format.html { redirect_to experiences_url, notice: 'Experience was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_experience
      @experience = Experience.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def experience_params
      params.require(:experience).permit(:profile_id, :user_id, :company_name, :designation, :from, :to)
    end
end
