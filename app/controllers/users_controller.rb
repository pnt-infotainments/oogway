class UsersController < ApplicationController
	before_action :find_user, except: :fetch_receiver
  # before_filter :ensure_signup_complete, only: [:new, :create, :update]
  def index
  	@posts = @user.posts.includes(:votes).page(params[:page]).per(2)
  end

  def following
  	following = Subscription.where(subscribe: 't',
                                   subscribable_type: 'User',
                                   user_id: @user
                                  ).pluck(:user_id)
    @users = User.where(id: following)
  end

  def followers
    followers = Subscription.where(subscribe: 't',
                                   subscribable_type: 'User',
                                   subscribable_id: @user
                                  ).pluck(:user_id)
  	@users = User.where(id: followers)
  end

  def subscription
    @following_posts = 
    Post.includes(:postable, :votes, :user, :post_images, tags: :taggings
                 ).joins("INNER JOIN channels
                          ON posts.postable_id = channels.id
                          AND posts.postable_type = 'Channel' 
                          INNER JOIN subscriptions sp
                          ON sp.subscribable_id = channels.id
                          AND sp.subscribable_type = 'Channel'"
                        ).where("sp.subscribe= 't'
                                AND sp.user_id = ?", @user)
                     #.select('"posts".*, "channels".id, "channels".name')
  end

  def manage_subscriptions
    @subscriptions = Channel.joins("INNER JOIN subscriptions sp
                       ON sp.subscribable_id = channels.id
                       AND sp.subscribable_type = 'Channel'"
                     ).where("sp.subscribe= 't'
                              AND sp.user_id = ?", @user)
  end
  
  def finish_signup
    # authorize! :update, @user 
    if request.patch? && params[:user] #&& params[:user][:email]
      if @user.update(user_params)
        # @user.skip_reconfirmation!
        sign_in(@user, :bypass => true)
        redirect_to root_url, notice: 'Your profile was successfully updated.'
      else
        @show_errors = true
      end
    end
  end

  def pending
    @assignments = Assignment.joins('LEFT OUTER JOIN "attachments" 
                    ON attachments.attachable_id = assignments.id'
                    ).joins(klass: [:user, :members]
                    ).where('assignments.due_date > now()'
                    ).where('attachments.link' => nil
                    ).where('members.user_id' => current_user,
                            'members.status' => true,
                            'members.block' => false)
                            
  end

  def completed
    @assignments = Assignment.joins(:attachments, klass: [:user, :members]
                    ).where('assignments.due_date >= now() 
                            AND assignments.due_date >= attachments.updated_at 
                            AND attachments.link IS NOT NULL'
                           ).where('members.user_id' => current_user,
                                   'members.status' => true,
                                   'members.block' => false)
    
  end

  def incomplete
    @assignments = Assignment.joins('LEFT OUTER JOIN "attachments" 
                    ON attachments.attachable_id = assignments.id'
                    ).joins(klass: [:user, :members]
                    ).where('assignments.due_date >= now() 
                            AND assignments.due_date < attachments.updated_at 
                            AND attachments.link IS NOT NULL'
                    ).where('members.user_id' => current_user,
                            'members.status' => true,
                            'members.block' => false)
      
  end

  def my_classes
    @my_classes = @user.my_classes
  end
  
  def subscribe
    SubscribeService.new({user: @user,
                          current_user: current_user 
                          }).follow
  end

  def unfollow
    SubscribeService.new({user: @user,
                          current_user: current_user 
                          }).unfollow
  end

  def fetch_receiver
    users = User.select([:id, :user_name]
              ).where('user_name ilike ?', 
                      "%#{params[:user_name]}%")
    if users.compact.empty?
      users = ['User not found.']
    else
      users = users.map(&:user_name)
    end
    render json: users 
  end

  private
  
  def find_user
  	@user = if params[:user_name].to_i.zero?
              User.find_by_user_name(params[:user_name])
            else
              User.find_by_id(params[:user_name])
            end
  end
  
  def user_params
    # accessible = []
    # accessible << [:email] # extend with your own params
    # accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
    params[:user][:user_name] = params[:user][:user_name].try(:downcase).try(:squish)
    params.require(:user).permit(:email, :user_name)
  end
end

# SELECT "posts".* 
# FROM "posts" 
# INNER JOIN "channels" 
# ON "channels"."id" = "posts"."channel_id" 
# INNER JOIN subscriptions 
# ON subscriptions.subscribable_id = channels.id 
# AND subscriptions.subscribable_type='Channel' 
# AND subscriptions.user_id = 1

