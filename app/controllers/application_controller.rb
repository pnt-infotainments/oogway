class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = exception.message
    redirect_to root_url
  end

  # rescue_from ActionView::MissingTemplate, with: :render_404
  rescue_from ActiveRecord::RecordNotFound, with: :render_404
  rescue_from ActionController::ParameterMissing, with: :error_during_processing
  rescue_from ActiveRecord::RecordInvalid, with: :error_during_processing
  # def ensure_signup_complete
  #   # Ensure we don't go into an infinite loop
  #   return if action_name == 'finish_signup'

  #   # Redirect to the 'finish_signup' page if the user
  #   # email hasn't been verified yet
  #   if curreauthorize! :show, @projectnt_user && !current_user.email_verified?
  #     redirect_to finish_signup_path(current_user)
  #   end
  # end
  def track_activity(track_obj, action=params[:action])
    current_user.activities.create!(action: action, trackable: track_obj)
  end
       
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :user_name
    devise_parameter_sanitizer.for(:sign_up) << :role_ids
    devise_parameter_sanitizer.for(:sign_in) << :user_name
  end

  def check_admin_permission
    unless current_user.has_role?(:administrator)
      flash[:error] = 'Access Denied'
      redirect_to root_url
    end  
  end

end
