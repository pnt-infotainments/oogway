class PostTypesController < ApplicationController
  def index
    @post_types = PostType.page(params[:page]).per(5)
  end
  
  def show
  	@post_type = PostType.find_by_slug(params[:id])
  end
end
