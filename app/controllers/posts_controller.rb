class PostsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show, :latest_post]
  before_action :set_post, only: [:edit, :destroy, 
                                  :update, :like,
                                  :dislike]
  before_action :find_postable, except: [:create]
  
  # GET /posts
  # GET /posts.json
  def index
    if params[:tag]
      @posts = Post.includes(:channel, :votes, 
                             :user, :post_images
                            ).tagged_with(params[:tag]
                                         ).active.order('updated_at DESC').page(params[:page]).per(5)
    else  
      @posts = if @postable
                  @postable.posts.includes(:votes, 
                                           :user, :post_images
                                          ).active.order('updated_at DESC').page(params[:page]).per(5)
               else
                  Post.includes(:votes, 
                                :user, :post_images
                               ).active.order('updated_at DESC').page(params[:page]).per(5)
               end
    end
    @channels = Channel.all
    @tags = Tag.popular_post.first(20)
  end

  def my
    @posts = current_user.posts.includes(:votes).page(params[:page]).per(5)
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @tags = Tag.popular_post.first(20)
    @post     = Post.includes(:votes, :post_images, :attachments,
                               :comments, :tags, :user
                            ).find_by_slug(params[:slug])
    @comments = @post.comments
    @comment  = @post.comments.new 
    @postable = @post.postable
    @more_posts = @postable.posts.includes(:votes, :post_images,
                                          :attachments, :comments,
                                          :tags, :user
                                         ).where('id != ?',
                                                  @post)
  end

  # GET /posts/new
  def new
    return redirect_to(posts_path, 
                       notice: 'Post can not be created without channel'
                      ) if @postable.nil?
    @post = @postable.posts.new
    # @contributors = User.joins(posts: :postable
    #                             ).select('users.*, count(posts.*) as post_count'
    #                                   ).group('users.id').order('post_count DESC')
    @post.post_images.build
    @post.attachments.build
  end

  # GET /posts/1/editcurrent_user
  def edit
    @post = @postable.posts.includes(:votes).find_by_slug(params[:slug])
    authorize! :update, @post
    @post.post_images.build if @post.post_images.empty?
    @post.attachments.build if @post.attachments.empty?
  end

  # POST /posts
  # POST /posts.json
  def create
    Rails.logger.info params.inspect
    @postable = fetch_postable
    return redirect_to(posts_path, 
                       notice: 'Post can not be created without channel'
                      ) if @postable.nil?
    @post = @postable.posts.new(post_params)
    @post.user = current_user
    @post.status = Status.find_by_name('Accepted')
    respond_to do |format|
      if @post.save
        track_activity(@post)
        format.html { redirect_to posts_path, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    authorize! :update, @post
    respond_to do |format|
      if @post.update(post_params)
        track_activity(@post)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    authorize! :destroy, @post
    @post.destroy
    track_activity(@post)
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def like
    post_params = {user: current_user, post: @post}
    PostService.new(post_params).like
    respond_to do |format|
      format.js
    end
  end

  def dislike
    post_params = {user: current_user, post: @post}
    PostService.new(post_params).dislike
    
    respond_to do |format|
      format.js
    end
  end

  def latest_post
    @latest_posts = Post.active.last(20)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.includes(:votes).find_by_slug(params[:slug])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      attachment_attr = params[:post][:attachments_attributes]['0']
      params[:post][:name] = params[:post][:name].squish
      params[:post][:description] = params[:post][:description].squish
      set_attachables if attachment_attr.key?(:link) || !attachment_attr[:link_cache].blank?
      params.require(:post).permit(:name, :description, :user_id, 
                                   :postable_id, :postable_type, :status_id, tag_list: [],
                                   attachments_attributes: [:link, :user_id, :attachable_id, 
                                                            :attachable_type, :link_cache, :_destroy,
                                                            :id],
                                   post_images_attributes: [:image, :image_cache, :_destroy, :id],
                                   post_contents_attributes: [:name, :description,
                                                              :_destroy, :id,
                                                              content_images_attributes: [:image, :image_cache, 
                                                                                          :_destroy, :id]])
    end

    def find_postable
      @postable = if params[:channel_id]
                    Channel.includes(:subscriptions
                      ).find_by_slug(params[:channel_id])
                  elsif params[:klass_id]
                    Klass.includes(:members
                      ).find_by_slug(params[:klass_id])
                  end
      @postable ||= @post.postable if @post
    end

    def fetch_postable
      if params[:post][:postable_type] == Channel.name
        Channel.includes(:subscriptions
          ).where('slug = :id OR id = :id', 
                  {id: params[:post][:postable_id]}
                 ).first
      elsif params[:post][:postable_type] == Klass.name
        Klass.includes(:members
          ).where('slug = :id OR id = :id',
                  {id: params[:post][:postable_id]}
                 ).first
      end
    end

    def set_attachables
      params[:post][:attachments_attributes]['0'][:user_id] = current_user.id
    end

    def add_channel
      Channel.find_by_name('Add_channel')      
    end
end
