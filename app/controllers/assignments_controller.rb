class AssignmentsController < ApplicationController
  before_action :find_assignable
  before_action :set_assignment, only: [:show, :edit, :update, :destroy]
  # load_and_authorize
  # GET /assignments
  # GET /assignments.json
  def index
    @assignments = @assignable.assignments
  end

  # GET /assignments/1
  # GET /assignments/1.json
  def show
  end

  def pending
    @assignments = Assignment.joins(:attachments, channel: [:user, :subscriptions]
                    ).where('assignments.due_date > now()'
                      ).where('subscriptions.user_id' => current_user,
                            'subscriptions.subscribe' => true,
                            'attachments.link' => nil)
                            
  end

  def completed
    @assignments = Assignment.joins(:attachments, channel: [:user, :subscriptions]
                    ).where('assignments.due_date >= now() 
                            AND assignments.due_date >= attachments.updated_at 
                            AND attachments.link IS NOT NULL'
                           ).where('subscriptions.user_id' => current_user,
                                   'subscriptions.subscribe' => true)
    
  end

  def incomplete
    @assignments = Assignment.joins(:attachments, channel: [:user, :subscriptions]
                    ).where('assignments.due_date >= now() 
                            AND assignments.due_date < attachments.updated_at 
                            AND attachments.link IS NOT NULL'
                           ).where('subscriptions.user_id' => current_user,
                                   'subscriptions.subscribe' => true)
    
  end

  # GET /assignments/new
  def new
    @assignment = @assignable.assignments.new
  end

  # GET /assignments/1/edit
  def edit
  end

  # POST /assignments
  # POST /assignments.json
  def create
    @assignment = @assignable.assignments.new(assignment_params)
    @assignment.user = current_user
    respond_to do |format|
      if @assignment.save
        format.html { redirect_to [@assignable, :assignments], notice: 'Assignment was successfully created.' }
        format.json { render :show, status: :created, location: @assignment }
      else
        format.html { render :new }
        format.json { render json: @assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assignments/1
  # PATCH/PUT /assignments/1.json
  def update
    respond_to do |format|
      if @assignment.update(assignment_params)
        format.html { redirect_to [@assignable, :assignments], notice: 'Assignment was successfully updated.' }
        format.json { render :show, status: :ok, location: @assignment }
      else
        format.html { render :edit }
        format.json { render json: @assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assignments/1
  # DELETE /assignments/1.json
  def destroy
    @assignment.destroy
    respond_to do |format|
      format.html { redirect_to [@assignable, :assignments], notice: 'Assignment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assignment
      @assignment = Assignment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def assignment_params
      params.require(:assignment).permit(:channel_id, :user_id, :title, :description, :due_date)
    end

    def find_assignable
      @assignable = if params[:type] == 'Klass' || params[:klass_id]
                      Klass.where('id = ? OR slug = ?', params[:klass_id].to_i, params[:klass_id]).first
                    elsif params[:type] == 'Channel' || params[:channel_id]
                      Channel.where('id = ? OR slug = ?', params[:channel_id].to_i, params[:channel_id]).first
                    end
    end
end
