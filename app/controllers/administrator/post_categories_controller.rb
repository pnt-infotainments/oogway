class Administrator::PostCategoriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_post_category, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /administrator/post_categories
  # GET /administrator/post_categories.json
  def index
    @post_categories = PostCategory.all
  end

  # GET /administrator/post_categories/1
  # GET /administrator/post_categories/1.json
  def show
  end

  # GET /administrator/post_categories/new
  def new
    @post_category = PostCategory.new
  end

  # GET /administrator/post_categories/1/edit
  def edit
  end

  # POST /administrator/post_categories
  # POST /administrator/post_categories.json
  def create
    @post_category = PostCategory.new(post_category_params)

    respond_to do |format|
      if @post_category.save
        format.html { redirect_to administrator_post_category_path(@post_category), notice: 'Post category was successfully created.' }
        format.json { render :show, status: :created, location: @post_category }
      else
        format.html { render :new }
        format.json { render json: @post_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /administrator/post_categories/1
  # PATCH/PUT /administrator/post_categories/1.json
  def update
    respond_to do |format|
      if @post_category.update(post_category_params)
        format.html { redirect_to administrator_post_category_path(@post_category), notice: 'Post category was successfully updated.' }
        format.json { render :show, status: :ok, location: @post_category }
      else
        format.html { render :edit }
        format.json { render json: @post_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /administrator/post_categories/1
  # DELETE /administrator/post_categories/1.json
  def destroy
    @post_category.destroy
    respond_to do |format|
      format.html { redirect_to administrator_post_categories_url, notice: 'Post category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def fetch_categories_for_type
    post_category = PostCategory.where(channel_id: params[:channel_id]).select([:id, :name])
    respond_to do |format|
      format.json { render json: post_category.to_json }
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post_category
      @post_category = PostCategory.find_by_slug(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_category_params
      params.require(:post_category).permit(:name, :description, :channel_id)
    end
end
