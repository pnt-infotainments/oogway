class Administrator::RolesController < ApplicationController
  before_action :set_administrator_role, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  
  # GET /administrator/roles
  # GET /administrator/roles.json
  def index
    @administrator_roles = Role.all
  end

  # GET /administrator/roles/1
  # GET /administrator/roles/1.json
  def show
  end

  # GET /administrator/roles/new
  def new
    @administrator_role = Role.new
  end

  # GET /administrator/roles/1/edit
  def edit
  end

  # POST /administrator/roles
  # POST /administrator/roles.json
  def create
    @administrator_role = Role.new(administrator_role_params)

    respond_to do |format|
      if @administrator_role.save
        format.html { redirect_to administrator_roles_path, notice: 'Role was successfully created.' }
        format.json { render :show, status: :created, location: @administrator_role }
      else
        format.html { render :new }
        format.json { render json: @administrator_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /administrator/roles/1
  # PATCH/PUT /administrator/roles/1.json
  def update
    respond_to do |format|
      if @administrator_role.update(administrator_role_params)
        format.html { redirect_to administrator_roles_path, notice: 'Role was successfully updated.' }
        format.json { render :show, status: :ok, location: @administrator_role }
      else
        format.html { render :edit }
        format.json { render json: @administrator_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /administrator/roles/1
  # DELETE /administrator/roles/1.json
  def destroy
    @administrator_role.destroy
    respond_to do |format|
      format.html { redirect_to administrator_roles_url, notice: 'Role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_administrator_role
      @administrator_role = Role.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def administrator_role_params
      params.require(:role).permit(:name)
    end
end
