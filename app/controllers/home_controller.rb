class HomeController < ApplicationController
  helper_method :resource_name, :resource, :devise_mapping
  def index
  	@posts = Post.includes(:channel, :votes, 
                           :user, :post_images
                          ).active.order('id desc').page(params[:page]).per(20)
    # if params[:tag]
   #    @posts = Post.includes(:channel, :votes, 
   #                           :user, :post_images
   #                          ).tagged_with(params[:tag]).page(params[:page]).per(5)
   #  else  
   #    @posts = if @channel
   #               @channel.posts.includes(:channel, :votes, 
   #                                       :user, :post_images).active.page(params[:page]).per(5)
   #             else
   #                Post.includes(:channel, :votes, 
   #                              :user, :post_images).active.page(params[:page]).per(5)
   #             end
   #  end
    @channels = {}
    @channels[:latest]  = Channel.last(20)
    @channels[:popular] = Channel.joins(:subscriptions
                                       ).select('channels.*,
                                                 count(subscriptions.*) 
                                                 as subscribed_count'
                                                ).group('channels.id'
                                                       ).order('subscribed_count DESC'
                                                              ).limit(20)
    @contributors = User.joins(:posts
                              ).select('users.*, count(posts.*) as post_count'
                                      ).group('users.id').order('post_count DESC')
    @weekly_hits = {}
    @weekly_hits[:posts] = Post.joins(:votes
                              ).select('posts.*, 
                                        count(votes.*) as votes_count'
                                      ).where('votes.vote_flag =? 
                                              AND posts.updated_at BETWEEN ? AND ?',
                                              true, Time.now - 1.year, Time.now
                                             ).order('votes_count DESC'
                                                    ).group('posts.id')
    @weekly_hits[:channels] = (Channel.joins(:subscriptions)
                                      .select('channels.*, count(subscriptions.*) as subscribed_count')
                                      .where('subscriptions.subscribe = ? AND channels.updated_at BETWEEN ? AND ?',
                                             true, Time.now - 1.week,
                                             Time.now).group('channels.id').order('subscribed_count DESC'))
    # @posts = Post.includes(:votes, :post_images).joins(channel: :channel_type).select("posts.*, channels.id, channels.name as channel_name, channel_types.id, channel_types.title as channel_type").group('channel_type')
  end

  def help

  end
  
  def to_do

  end

  def browse_posts
    @posts = Post.where("lower(name) ilike ?",
                         [params[:char].squish, '%'].join
                       ).page(params[:page])
  end

  def browser_channel

  end

  def browse_tags
    @tags = {} 
    @tags[:list] = Tag.list
    @tags[:popular_post] = Tag.popular_post(params[:name], params[:char])
  end
  
  def tags
    @posts = Post.includes(:channel, :votes, 
                             :user, :post_images
                            ).tagged_with(params[:tag]
                                         ).order('updated_at DESC').page(params[:page])
  end
  
  def join_us
  end

  def resource_name
    :user
  end
 
  def resource
    @resource ||= User.new
  end
 
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

end
