class ProfilesController < ApplicationController
  before_action :authenticate_user!, except: [:basic_info,
                                              :social_info,
                                              :school_info,
                                              :college_info,
                                              :experience,
                                              :project, :activity,
                                              :achievement, 
                                              :show, :contributions]
  before_action :find_user
  before_action :set_profile, only: [:index, :show, 
                                     :edit, :contributions,
                                     :education, 
                                     :experience, :destroy]
  before_action :profile_info, only: [:basic_info,
                                      :social_info,
                                      :school_info,
                                      :college_info,
                                      :experience,
                                      :project,
                                      :achievement,
                                      :contributions]

  # GET /profiles
  # GET /profiles.json
  def index
    unless @profile
      redirect_to new_profile_path, notice: 'Please create your profile'
    else
      @posts = @user.posts.includes(:votes).page(params[:page]).per(2)
    end
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
    redirect_to(new_profile_path, 
                notice: 'Please create your profile') unless @profile
    @school_details = @profile.try(:school_details)
  end

  # GET /profiles/new
  def new
    @profile = current_user.profile
    redirect_to basic_info_profile_path(current_user.user_name || current_user) unless @profile
  end

  def basic_info
    respond_to do |format|
      format.html
      format.js
    end
  end

  def school_info
    render :basic_info if @profile.try(:first_name).nil?
    @school_details = @profile.try(:school_details)
    # if @school_details.blank? 
    #   @school_details = @profile.school_details.new
    # end 
    respond_to do |format|
      format.html
      format.js
    end
  end

  def college_info
    @college_details = @profile.try(:college_details)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def social_info
    respond_to do |format|
      format.html
      format.js
    end
  end

  def experience
    @experiences = @profile.try(:experiences)
    respond_to do |format|
      format.js
    end
  end

  def project
    @projects = @profile.try(:projects)
    respond_to do |format|
      format.js
    end
  end

  def achievement
    @achievements = @profile.try(:achievements)
    respond_to do |format|
      format.js
    end
  end

  def activity
    @user = fetch_user || current_user
    @activities = @user.activities.includes(:trackable).order('created_at DESC').all
  end

  def education
    @college_detail = @profile.college_details
    if @college_detail.blank?
      @college_detail = CollegeDetail.new(user_id: current_user,
                                          profile_id: @profile)
    end
    @school_detail = @profile.school_details
    if @school_detail.blank?
      @school_detail = SchoolDetail.new(user_id: current_user,
                                        profile_id: @profile)
    end
  end

  def contributions
    @posts = @user.posts.includes(:votes, :tags).page(params[:page])
  end

  def channels
    @channels = @user.channels
  end

  # GET /profiles/1/edit
  def edit
    @profile.college_details.build
    # @profile.school_details.build
    @profile.achievements.build
    @profile.experiences.build
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params)
    @profile.user = current_user
    respond_to do |format|
      if @profile.save
        track_activity(@profile)
        format.html { redirect_to @profile, notice: 'Profile was successfully created.' }
        format.json { render :show, status: :created, location: @profile }
      else
        format.html { render :new }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    @profile = Profile.where('user_name =? or id =?',
                             params[:user_name], params[:user_name]).first
    respond_to do |format|
      if @profile.update(profile_params)
        track_activity(@profile)
        format.html { redirect_to profile_path(current_user.user_name), notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profile
      @user = fetch_user
      @profile = Profile.includes(:interests, :skills
                  ).where(user_id: @user).first if @user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profile_params
      params.require(:profile).permit(:user_id, :first_name,
                                      :avatar, :avatar_cache,      
                                      :cover_photo, :cover_photo_cache, 
                                      :last_name, :facebook,
                                      :github, :linkedin,
                                      :college_id, :about,
                                      interested_in: [],
                                      skill_list: [],
                                      experience_attributes: [:user_id, :company_name,
                                                              :designation, :from, :to,
                                                              :_destroy, :id],
                                      achievement_attributes: [:title, :description, 
                                                               :user_id, :id,
                                                               :_destroy],
                                      college_details_attributes: [:degree, :college_id, 
                                                                   :year, :department,
                                                                   :user_id, :id,
                                                                   :_destroy],
                                      school_details_attributes: [:name, :user_id, 
                                                                  :class, :_destroy, :id])
    end

    def find_user
      @user = fetch_user
    end

    def profile_info
      @profile = @user.profile
      @profile = Profile.new unless @profile
    end

    def fetch_user
      User.where('user_name = ? OR id = ?',
                 params[:user_name],
                 params[:id]).first || current_user 
    end
end
