$(document).ready(function(){
	$('#post_channel_id').change(function(){
		channel_id = $(this).val();
		var url = 
			'/administrator/post_categories/fetch_categories_for_type?channel_id=' + $(this).val() + '';
		$.ajax({
	    url: url,
	    type: 'GET',
	    success: function(data){ 
	    	$('#post_post_category_id').empty();
	    	$('#post_post_category_id').append($('<option>').attr('value', '').text('Please Select'));
	    	$.each(data, function (i, object) {
          $('#post_post_category_id').append($('<option>').attr('value', object['id']).text(object['name']));
        });
	    }
  	});
		$('#post_post_category_id').attr('disabled',false)
	})
})