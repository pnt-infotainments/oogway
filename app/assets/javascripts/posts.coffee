# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->

	$('#post_tag_list').select2
	  tags: true,
	  placeholder: 'Tags'

	$('.best_in_place').best_in_place

	
	$('form').on 'click', '.remove_fields', (event) ->
		$(this).prev('input[type=hidden]').val('true')
		$(this).closest('fieldset').hide()
		event.preventDefault()
		
	$('form').on 'click', '.add_fields', (event) ->
		if $('.remove_fields').length > 6
	  	alert("reached max length")
	  	return false
	  time = new Date().getTime()
		regexp = new RegExp($(this).data('id'), 'g')
		$(this).before($(this).data('fields').replace(regexp, time))
		tinymce.init
	    selector: 'textarea'
	    menubar: false
	    browser_spellcheck: true
	    plugins: [ 'print preview emoticons' ]
	    toolbar: 'preview | print | emoticons | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent'
	  
  
		event.preventDefault()

	# $(window).scroll ->
	# 	win = $(window)
	# 	url = $('.pagination').find('a[rel=next]').attr('href')
	# 	if $(document).height() - win.height() == win.scrollTop()
  #    $('.pagination').text('Fetching more posts')
  #    $('#loading').show()
  #    $.ajax
  #      url: url,
  #      dataType: 'script'
        

	stickySidebar = $('.sticky')
	if stickySidebar.length > 0
	  stickyHeight = stickySidebar.height()
	  sidebarTop = stickySidebar.offset().top
	# on scroll move the sidebar
	$(window).scroll ->
	  if stickySidebar.length > 0
	    scrollTop = $(window).scrollTop()
	    if sidebarTop < scrollTop
	      stickySidebar.css 'top', scrollTop - sidebarTop
	      # stop the sticky sidebar at the footer to avoid overlapping
	      sidebarBottom = stickySidebar.offset().top + stickyHeight
	      stickyStop = $('.body-section').offset().top + $('.body-section').height()
	      if stickyStop < sidebarBottom
	        stopPosition = $('.body-section').height() - stickyHeight
	        stickySidebar.css 'top', stopPosition
	    else
	      stickySidebar.css 'top', '0'
	  return
	$(window).resize ->
	  if stickySidebar.length > 0
	    stickyHeight = stickySidebar.height()
	  return
