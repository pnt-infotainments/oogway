# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
	$('#channel_search').keyup ->
	  searchText = $(this).val().toLowerCase()
	  $('#channel_list').find('li').each ->
	    currentLiText = $(this).text().toLowerCase()
	    showCurrentLi = currentLiText.indexOf(searchText) >= 0
	    $(this).toggle showCurrentLi
	    return
	  return
