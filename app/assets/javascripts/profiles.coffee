# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
	
	$('#profile_interested_in').select2
		tags: true,
		placeholder: 'Interested In'

	$('#profile_skill_list').select2
		tags: true,
		placeholder: 'Key Skills'
	
	$('#profile_college_id').select2()
	
