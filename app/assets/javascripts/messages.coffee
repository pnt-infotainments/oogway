# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
	$('#message_recipient_id').autocomplete
		minLength: 3,
		source: (request, response) ->
	    $.get "/users/#{request.term}/fetch_receiver", (data) ->
	      response data
	      return


	$('#preview').click ->
		receiver = $('#message_recipient_id').val()
		subject = $('#message_subject').val()
		mgs_body = $('#message_body').val()
		$('.receiver').html(receiver)
		$('#subject').html(subject)
		$('#msg_body').html(mgs_body)
