// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require jquery.purr
//= require best_in_place
//= require foundation
//= require wice_grid
//= require select2
//= require slick.min.js
//= require_tree .

$(function(){ 
	$(document).foundation(); 
	if (!Modernizr.touch || !Modernizr.inputtypes.date) {
	  $('input[type=date]')
	    .attr('type', 'text')
	    .datepicker({
	      // Consistent format with the HTML5 picker
	      dateFormat: 'yy-mm-dd'
	    });
	}
});

