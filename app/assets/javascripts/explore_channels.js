$(document).ready(function(){
		$('.slider-for').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  arrows: false,
		  fade: true,
		  asNavFor: '.slider-nav'
		});
		$('.slider-nav').slick({
		  slidesToShow: 5,
		  slidesToScroll: 1,
		  asNavFor: '.slider-for',
		  dots: false,
		  centerMode: true,
		  infinite: true,
		  focusOnSelect: true,
		  responsive: [
								    {
								      breakpoint: 1024,
								      settings: {
								        slidesToShow: 3,
								        slidesToScroll: 3,
								        infinite: true,
								        dots: false
								      }
								    },
								    {
								      breakpoint: 600,
								      settings: {
								        slidesToShow: 2,
								        slidesToScroll: 2
								      }
								    },
								    {
								      breakpoint: 500,
								      settings: {
								        slidesToShow: 1,
								        slidesToScroll: 1
								      }
								    }
									    // You can unslick at a given breakpoint now by adding:
									    // settings: "unslick"
									    // instead of a settings object
		  						]
		});
	})
