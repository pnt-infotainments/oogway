class MemberService
	
	def initialize(user, klass)
		@user = user
		@klass = klass
	end

	def join_class
		klass_member = set_member
		if verify_member(klass_member)
			klass_member.user_id = @user.id
			klass_member.status = true
			klass_member.save
			return true
		end
		return false
	end

	def verify_member(klass_member)
		return false if klass_member.status || klass_member.block
		return false if klass_member.user_id == @user.id
		return true
	end

	def set_member
		member = @klass.members.find_by_user_id(@user)
		member ||= @klass.members.new
	end
end
