class PostService
	def initialize(params)
		@user = params[:user]
		@post = params[:post]
	end

	def like
		unless user.voted_up_on? post
      user.likes post
      track_activity(post, 'like') 
    end
	end

	def dislike
		unless user.voted_down_on? post
      user.dislikes post
      track_activity(post, 'like') 
    end
	end

	private

  attr_reader :user, :post

  def track_activity(track_obj, action=nil)
    user.activities.create!(action: action, trackable: track_obj)
  end
end
