class SubscribeService
	def initialize(params)
		@channel = params[:channel]
		@follow_user = params[:user]
		@current_user = params[:current_user]
	end

	def subscribe
		subscribable						= channel
    subscription            = find_subcription(subscribable) 
    subscription            ||= subscribable.subscriptions.new
    subscription.user_id    = current_user.id
    subscription.email      = current_user.email
    subscription.subscribe  = true
    subscription.save
    track_activity(subscribable, 'subscribe')
	end

	def unsubscribe
		subscribable						= channel
    subscription            = find_subcription(subscribable)
    subscription.update_column(:subscribe, false)
    track_activity(subscribable, 'unsubscribe')
	end

	def follow
		subscribable						= follow_user
    subscription            = follower(subscribable) 
    subscription            ||= subscribable.followers.new
    subscription.user_id    = current_user.id
    subscription.email      = current_user.email
    subscription.subscribe  = true
    subscription.save
    track_activity(subscribable, 'follow')
	end

	def unfollow
		subscribable						= follow_user
    subscription            = follower(subscribable)
    subscription.update_column(:subscribe, false)
    track_activity(subscribable, 'unfollow')
	end
	
	private

	attr_reader :channel, :follow_user, :current_user

	def find_subcription(obj)
		obj.subscriptions.find_by_user_id(current_user)
	end

	def follower(obj)
		obj.followers.find_by_user_id(current_user)
	end

	def track_activity(track_obj, action=nil)
    current_user.activities.create!(action: action, trackable: track_obj)
  end

end
