json.extract! @profile, :id, :user_id, :first_name, :last_name, :facebook, :github, :linkedin, :education, :job_details, :created_at, :updated_at
