json.array!(@school_details) do |school_detail|
  json.extract! school_detail, :id, :user_id, :profile_id, :class, :name
  json.url school_detail_url(school_detail, format: :json)
end
