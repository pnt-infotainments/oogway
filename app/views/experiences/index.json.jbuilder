json.array!(@experiences) do |experience|
  json.extract! experience, :id, :profile_id, :user_id, :company_name, :designation, :from, :to
  json.url experience_url(experience, format: :json)
end
