json.array!(@answers) do |answer|
  json.extract! answer, :id, :channel_id, :user_id, :contents, :post_id, :question_id
  json.url answer_url(answer, format: :json)
end
