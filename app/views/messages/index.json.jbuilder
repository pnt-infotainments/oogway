json.array!(@messages) do |message|
  json.extract! message, :id, :recipient_id, :sender_id, :body, :subject
  json.url message_url(message, format: :json)
end
