json.array!(@klasses) do |klass|
  json.extract! klass, :id, :title, :description, :code, :user_id
  json.url klass_url(klass, format: :json)
end
