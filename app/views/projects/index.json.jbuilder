json.array!(@projects) do |project|
  json.extract! project, :id, :user_id, :profile_id, :title, :description, :duration, :role, :team_size
  json.url project_url(project, format: :json)
end
