json.array!(@administrator_roles) do |administrator_role|
  json.extract! administrator_role, :id
  json.url administrator_role_url(administrator_role, format: :json)
end
