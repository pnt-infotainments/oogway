json.array!(@administrator_post_categories) do |administrator_post_category|
  json.extract! administrator_post_category, :id, :name, :description
  json.url administrator_post_category_url(administrator_post_category, format: :json)
end
