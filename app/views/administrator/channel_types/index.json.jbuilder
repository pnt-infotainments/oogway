json.array!(@channel_types) do |post_type|
  json.extract! channel_type, :id, :title, :description
  json.url channel_type_url(channel_type, format: :json)
end
