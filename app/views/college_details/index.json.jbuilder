json.array!(@college_details) do |college_detail|
  json.extract! college_detail, :id, :degree, :college_id, :year, :department, :profile_id, :user_id
  json.url college_detail_url(college_detail, format: :json)
end
