json.array!(@questions) do |question|
  json.extract! question, :id, :channel_id, :contents, :user_id, :post_id
  json.url question_url(question, format: :json)
end
