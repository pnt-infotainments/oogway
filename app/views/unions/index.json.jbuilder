json.array!(@unions) do |union|
  json.extract! union, :id, :name, :sub_domain
  json.url union_url(union, format: :json)
end
