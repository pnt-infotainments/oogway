json.array!(@channel_images) do |channel_image|
  json.extract! channel_image, :id, :name, :description, :channel_id
  json.url channel_image_url(channel_image, format: :json)
end
