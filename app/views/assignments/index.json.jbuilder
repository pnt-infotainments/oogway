json.array!(@assignments) do |assignment|
  json.extract! assignment, :id, :channel_id, :user_id, :title, :description, :due_date
  json.url assignment_url(assignment, format: :json)
end
