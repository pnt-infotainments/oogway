json.array!(@comments) do |comment|
  json.extract! comment, :id, :content, :user_id, :report_abuse, :email, :commentable
  json.url comment_url(comment, format: :json)
end
