json.array!(@channel_types) do |channel_type|
  json.extract! channel_type, :id, :channel_id, :title, :description, :image
  json.url channel_type_url(channel_type, format: :json)
end
