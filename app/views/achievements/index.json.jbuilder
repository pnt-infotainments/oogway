json.array!(@achievements) do |achievement|
  json.extract! achievement, :id, :profile_id, :user_id, :title, :description
  json.url achievement_url(achievement, format: :json)
end
