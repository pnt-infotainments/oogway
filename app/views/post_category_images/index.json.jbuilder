json.array!(@post_category_images) do |post_category_image|
  json.extract! post_category_image, :id, :name, :description, :post_category_id
  json.url post_category_image_url(post_category_image, format: :json)
end
