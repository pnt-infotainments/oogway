json.array!(@members) do |member|
  json.extract! member, :id, :user_id, :status, :end_date, :block, :klass_id, :remarks
  json.url member_url(member, format: :json)
end
