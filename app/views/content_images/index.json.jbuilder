json.array!(@content_images) do |content_image|
  json.extract! content_image, :id, :image, :image_url, :post_content_id, :post_id, :channel_id
  json.url content_image_url(content_image, format: :json)
end
