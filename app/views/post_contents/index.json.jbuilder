json.array!(@post_contents) do |post_content|
  json.extract! post_content, :id, :content, :post_id, :post_type_id
  json.url post_content_url(post_content, format: :json)
end
