module PostsHelper
	def nested_comments(comments)
		comments.map do |comment, sub_commment|
			content_tag(:div, render(comment), class: 'comment push'
								 ) + content_tag(:div, nested_comments(sub_commment), 
								 								 class: 'indented comment push')
		end.join.html_safe
	end

end
