require 'rails_helper'

RSpec.describe "assignments/new", type: :view do
  before(:each) do
    assign(:assignment, Assignment.new(
      :channel => nil,
      :user => nil,
      :title => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new assignment form" do
    render

    assert_select "form[action=?][method=?]", assignments_path, "post" do

      assert_select "input#assignment_channel_id[name=?]", "assignment[channel_id]"

      assert_select "input#assignment_user_id[name=?]", "assignment[user_id]"

      assert_select "input#assignment_title[name=?]", "assignment[title]"

      assert_select "textarea#assignment_description[name=?]", "assignment[description]"
    end
  end
end
