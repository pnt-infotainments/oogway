require 'rails_helper'

RSpec.describe "klasses/edit", type: :view do
  before(:each) do
    @klass = assign(:klass, Klass.create!(
      :title => "MyString",
      :description => "MyText",
      :code => "MyString",
      :user => nil
    ))
  end

  it "renders the edit klass form" do
    render

    assert_select "form[action=?][method=?]", klass_path(@klass), "post" do

      assert_select "input#klass_title[name=?]", "klass[title]"

      assert_select "textarea#klass_description[name=?]", "klass[description]"

      assert_select "input#klass_code[name=?]", "klass[code]"

      assert_select "input#klass_user_id[name=?]", "klass[user_id]"
    end
  end
end
