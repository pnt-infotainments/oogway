require 'rails_helper'

RSpec.describe "klasses/index", type: :view do
  before(:each) do
    assign(:klasses, [
      Klass.create!(
        :title => "Title",
        :description => "MyText",
        :code => "Code",
        :user => nil
      ),
      Klass.create!(
        :title => "Title",
        :description => "MyText",
        :code => "Code",
        :user => nil
      )
    ])
  end

  it "renders a list of klasses" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Code".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
