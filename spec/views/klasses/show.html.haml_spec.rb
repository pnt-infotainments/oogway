require 'rails_helper'

RSpec.describe "klasses/show", type: :view do
  before(:each) do
    @klass = assign(:klass, Klass.create!(
      :title => "Title",
      :description => "MyText",
      :code => "Code",
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Code/)
    expect(rendered).to match(//)
  end
end
