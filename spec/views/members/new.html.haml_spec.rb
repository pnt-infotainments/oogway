require 'rails_helper'

RSpec.describe "members/new", type: :view do
  before(:each) do
    assign(:member, Member.new(
      :user => nil,
      :status => false,
      :block => false,
      :klass => nil,
      :remarks => "MyString"
    ))
  end

  it "renders new member form" do
    render

    assert_select "form[action=?][method=?]", members_path, "post" do

      assert_select "input#member_user_id[name=?]", "member[user_id]"

      assert_select "input#member_status[name=?]", "member[status]"

      assert_select "input#member_block[name=?]", "member[block]"

      assert_select "input#member_klass_id[name=?]", "member[klass_id]"

      assert_select "input#member_remarks[name=?]", "member[remarks]"
    end
  end
end
