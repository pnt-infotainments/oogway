require 'rails_helper'

RSpec.describe "members/index", type: :view do
  before(:each) do
    assign(:members, [
      Member.create!(
        :user => nil,
        :status => false,
        :block => false,
        :klass => nil,
        :remarks => "Remarks"
      ),
      Member.create!(
        :user => nil,
        :status => false,
        :block => false,
        :klass => nil,
        :remarks => "Remarks"
      )
    ])
  end

  it "renders a list of members" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Remarks".to_s, :count => 2
  end
end
