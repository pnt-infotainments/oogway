require "rails_helper"

RSpec.describe Post, type: :model do
  
  context 'post has many votes' do
    it "should have many votes" do
      has_many_association(:votes)
    end
  end
  
  context 'post has many post images' do
    it "should have many post images" do
      has_many_association(:post_images)
    end
  end
  
  context 'post has many subscriptions' do
    it "should have many subscriptions" do
      has_many_association(:subscriptions)
    end
  end

  context 'post has many comments' do
    it "should have many comments" do
      has_many_association(:comments)
    end
  end

  context 'post has many taggings' do
    it "should have many taggings" do
      has_many_association(:taggings)
    end
  end

  context 'post has many attachments' do
    it "should have many attachments" do
      has_many_association(:attachments)
    end
  end

  context 'post belongs to channel' do
    it "should belong to channel" do
      belongs_to_association(:channel)
    end
  end

  context 'post belongs to user' do
    it "should belong to user" do
      belongs_to_association(:user)
    end
  end
  
  context 'post belongs to user' do
    it "should belong to user" do
      belongs_to_association(:status)
    end
  end

  describe 'Post along with channel' do
    let(:channel) { Channel.create!(name: 'Rspec Channel',
                                    description: 'New channel description'
                                   )}
    let(:post) { Post.create!(name: 'Rspec post',
                              description: 'New post description',
                              channel_id: channel.id) }
    let!(:comment) { post.comments.create content: 'first comment' }
    let!(:tag_list) { post.tag_list=('rspec tag') }
  	
    it "creates a post" do
  		expect(post.present?).to eq(true)
  	end

    it "creates a comment" do
      post.comments.create(content: "second comment")
      expect(post.reload.comments.size).to eq(2)
    end

    it "creates a tag for post" do
      expect(post.tag_list).to eq(['rspec tag'].join(', '))
    end

    it "creates multiple tags for post" do
      post.tag_list=(['rspec tag 1','rspec tag 2'])
      expect(post.tag_list).to eq(['rspec tag 1','rspec tag 2'].join(', '))
    end
    
    it "finds post tagged with" do
      post.tag_list=(['rspec tag 1','rspec tag 2'])
      tagged_posts = Post.tagged_with('rspec tag 1')
      expect(tagged_posts.first.name).to eq('New post')
    end

    it "find post tagged with" do
      post.tag_list=(['rspec tag 1','rspec tag 2'])
      tagged_posts = Post.tagged_with('blah')
      expect(tagged_posts).to eq(nil)
    end    
  end

end

private

def has_many_association(model_action)
  t = Post.reflect_on_association(model_action)
  expect(t.macro).to eq(:has_many)
end

def belongs_to_association(model_action)
  t = Post.reflect_on_association(model_action)
  expect(t.macro).to eq(:belongs_to)
end

