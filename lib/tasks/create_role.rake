
namespace :create_role do
	desc 'Creating default roles'
	task :setup_roles do 
		['Student', 'Alumini', 'Mentor'].each do |role|
			Role.where(name: role).first_or_create!
		end
	end
end
