require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test "registered" do
    mail = UserMailer.registered
    assert_equal "Registered", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "new_follower" do
    mail = UserMailer.new_follower
    assert_equal "New follower", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
