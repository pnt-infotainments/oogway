# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/registered
  def registered
    UserMailer.registered
  end

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/new_follower
  def new_follower
    UserMailer.new_follower
  end

end
