# Preview all emails at http://localhost:3000/rails/mailers/channel_mailer
class ChannelMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/channel_mailer/created
  def created
    ChannelMailer.created
  end

  # Preview this email at http://localhost:3000/rails/mailers/channel_mailer/accepted
  def accepted
    ChannelMailer.accepted
  end

  # Preview this email at http://localhost:3000/rails/mailers/channel_mailer/rejected
  def rejected
    ChannelMailer.rejected
  end

  # Preview this email at http://localhost:3000/rails/mailers/channel_mailer/followed
  def followed
    ChannelMailer.followed
  end

  # Preview this email at http://localhost:3000/rails/mailers/channel_mailer/unfollow
  def unfollow
    ChannelMailer.unfollow
  end

end
