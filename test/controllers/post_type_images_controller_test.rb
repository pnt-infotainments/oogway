require 'test_helper'

class ChannelImagesControllerTest < ActionController::TestCase
  setup do
    @channel_image = channel_images(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:channel_images)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create channel_image" do
    assert_difference('ChannelImage.count') do
      post :create, channel_image: { description: @channel_image.description, name: @channel_image.name, channel_id: @channel_image.channel_id }
    end

    assert_redirected_to channel_image_path(assigns(:channel_image))
  end

  test "should show channel_image" do
    get :show, id: @channel_image
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @channel_image
    assert_response :success
  end

  test "should update channel_image" do
    patch :update, id: @channel_image, channel_image: { description: @channel_image.description, name: @channel_image.name, channel_id: @channel_image.channel_id }
    assert_redirected_to channel_image_path(assigns(:channel_image))
  end

  test "should destroy channel_image" do
    assert_difference('ChannelImage.count', -1) do
      delete :destroy, id: @channel_image
    end

    assert_redirected_to channel_images_path
  end
end
