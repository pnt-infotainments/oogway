require 'test_helper'

class PostCategoryImagesControllerTest < ActionController::TestCase
  setup do
    @post_category_image = post_category_images(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:post_category_images)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create post_category_image" do
    assert_difference('PostCategoryImage.count') do
      post :create, post_category_image: { description: @post_category_image.description, name: @post_category_image.name, post_category_id: @post_category_image.post_category_id }
    end

    assert_redirected_to post_category_image_path(assigns(:post_category_image))
  end

  test "should show post_category_image" do
    get :show, id: @post_category_image
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @post_category_image
    assert_response :success
  end

  test "should update post_category_image" do
    patch :update, id: @post_category_image, post_category_image: { description: @post_category_image.description, name: @post_category_image.name, post_category_id: @post_category_image.post_category_id }
    assert_redirected_to post_category_image_path(assigns(:post_category_image))
  end

  test "should destroy post_category_image" do
    assert_difference('PostCategoryImage.count', -1) do
      delete :destroy, id: @post_category_image
    end

    assert_redirected_to post_category_images_path
  end
end
