require 'test_helper'

class UserControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get following" do
    get :following
    assert_response :success
  end

  test "should get followers" do
    get :followers
    assert_response :success
  end

  test "should get subscription" do
    get :subscription
    assert_response :success
  end

end
