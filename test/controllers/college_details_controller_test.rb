require 'test_helper'

class CollegeDetailsControllerTest < ActionController::TestCase
  setup do
    @college_detail = college_details(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:college_details)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create college_detail" do
    assert_difference('CollegeDetail.count') do
      post :create, college_detail: { college_id: @college_detail.college_id, degree: @college_detail.degree, department: @college_detail.department, profile_id: @college_detail.profile_id, user_id: @college_detail.user_id, year: @college_detail.year }
    end

    assert_redirected_to college_detail_path(assigns(:college_detail))
  end

  test "should show college_detail" do
    get :show, id: @college_detail
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @college_detail
    assert_response :success
  end

  test "should update college_detail" do
    patch :update, id: @college_detail, college_detail: { college_id: @college_detail.college_id, degree: @college_detail.degree, department: @college_detail.department, profile_id: @college_detail.profile_id, user_id: @college_detail.user_id, year: @college_detail.year }
    assert_redirected_to college_detail_path(assigns(:college_detail))
  end

  test "should destroy college_detail" do
    assert_difference('CollegeDetail.count', -1) do
      delete :destroy, id: @college_detail
    end

    assert_redirected_to college_details_path
  end
end
