require 'test_helper'

class Administrator::RolesControllerTest < ActionController::TestCase
  setup do
    @administrator_role = administrator_roles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:administrator_roles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create administrator_role" do
    assert_difference('Administrator::Role.count') do
      post :create, administrator_role: {  }
    end

    assert_redirected_to administrator_role_path(assigns(:administrator_role))
  end

  test "should show administrator_role" do
    get :show, id: @administrator_role
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @administrator_role
    assert_response :success
  end

  test "should update administrator_role" do
    patch :update, id: @administrator_role, administrator_role: {  }
    assert_redirected_to administrator_role_path(assigns(:administrator_role))
  end

  test "should destroy administrator_role" do
    assert_difference('Administrator::Role.count', -1) do
      delete :destroy, id: @administrator_role
    end

    assert_redirected_to administrator_roles_path
  end
end
