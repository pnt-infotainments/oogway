require 'test_helper'

class Administrator::PostCategoriesControllerTest < ActionController::TestCase
  setup do
    @administrator_post_category = administrator_post_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:administrator_post_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create administrator_post_category" do
    assert_difference('Administrator::PostCategory.count') do
      post :create, administrator_post_category: { description: @administrator_post_category.description, name: @administrator_post_category.name }
    end

    assert_redirected_to administrator_post_category_path(assigns(:administrator_post_category))
  end

  test "should show administrator_post_category" do
    get :show, id: @administrator_post_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @administrator_post_category
    assert_response :success
  end

  test "should update administrator_post_category" do
    patch :update, id: @administrator_post_category, administrator_post_category: { description: @administrator_post_category.description, name: @administrator_post_category.name }
    assert_redirected_to administrator_post_category_path(assigns(:administrator_post_category))
  end

  test "should destroy administrator_post_category" do
    assert_difference('Administrator::PostCategory.count', -1) do
      delete :destroy, id: @administrator_post_category
    end

    assert_redirected_to administrator_post_categories_path
  end
end
