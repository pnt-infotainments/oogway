require 'test_helper'

class SchoolDetailsControllerTest < ActionController::TestCase
  setup do
    @school_detail = school_details(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:school_details)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create school_detail" do
    assert_difference('SchoolDetail.count') do
      post :create, school_detail: { class: @school_detail.class, name: @school_detail.name, profile_id: @school_detail.profile_id, user_id: @school_detail.user_id }
    end

    assert_redirected_to school_detail_path(assigns(:school_detail))
  end

  test "should show school_detail" do
    get :show, id: @school_detail
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @school_detail
    assert_response :success
  end

  test "should update school_detail" do
    patch :update, id: @school_detail, school_detail: { class: @school_detail.class, name: @school_detail.name, profile_id: @school_detail.profile_id, user_id: @school_detail.user_id }
    assert_redirected_to school_detail_path(assigns(:school_detail))
  end

  test "should destroy school_detail" do
    assert_difference('SchoolDetail.count', -1) do
      delete :destroy, id: @school_detail
    end

    assert_redirected_to school_details_path
  end
end
