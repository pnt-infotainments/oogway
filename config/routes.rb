require 'subdomain'
Rails.application.routes.draw do
   
  resources :attachments
  # resources :institutions
  # resources :klasses do
  #   get :explore, on: :collection 
  #   post :join, on: :member 
  #   resources :assignments
  #   resources :members
  #   resources :posts
  # end
  
  get 'search/post'
  post 'search/fetch_post'
  get 'search/user'

  namespace :administrator do
    resources :roles
  end
  resources :activities
  resources :colleges do
    post :import, on: :collection
  end
  resources :answers
  resources :questions
  resources :content_images
  # resources :post_contents
  resources :post_category_images
  resources :profiles, param: :user_name do
    member do
      get :education
      get :basic_info
      get :social_info
      get :school_info
      get :college_info
      get :experience
      get :achievement
      get :project
      get :activity
      get :contributions
      resources :achievements
      resources :projects
      resources :experiences, param: :id
      resources :school_details
      resources :college_details
    end
  end
  
  root 'home#index'
  get 'home' => 'home#index', as: 'home'
  get 'browse_posts' => 'home#browse_posts', as: 'browse_posts'
  get 'help' => 'home#help', as: 'help'
  get 'join_us' => 'home#join_us', as: 'join_us'
  get 'to_do' => 'home#to_do', as: 'to_do'
  get 'volunteering' => 'home#volunteering', as: 'volunteering'
  get 'tags/:tag', to: 'home#tags', as: :tag
  
  resources :subscriptions
  
  namespace :administrator do
    resources :channel_types
    resources :channels
    resources :statuses
    resources :post_categories do
      get :fetch_categories_for_type, on: :collection
    end
    resources :posts do
      get '(page/:page)', :action => :index, :on => :collection, :as => ''
    end
  end
  resources :channels, except: :destory do
    collection do
      get :list_channels
      get :explore
      get :browse
      get :recommended
      get :my
    end
    member do 
      post :subscribe
      post :unfollow
      resources :channel_images
      get :followers
    end
    get :new_index
    resources :assignments
    resources :posts, only: :index, on: :member
  end
  resources :posts, param: :slug do
    get :my, on: :collection
    get :latest_post, on: :collection
    member do
      post :like
      post :dislike
      resources :embed_codes
      resources :post_images
      resources :post_contents
      resources :comments do 
        post :report_abuse
        get :reply
      end
    end
     get '(page/:page)', :action => :index, :on => :collection, :as => ''
  end

  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks',
                                    registrations: 'users/registrations' }
  
  resources :users, param: :user_name do 
    collection do 
      get :pending
      get :completed
      get :incomplete
      get :sign_in_popup
    end
    member do
      get :following
      get :followers
      get :subscription
      get :my_classes
      get :manage_subscriptions
      get :fetch_receiver
      post :subscribe
      post :unfollow
      resources :messages do
        get '(page/:page)', :action => :index, :on => :collection, :as => ''
      end
    end
  end
  
  match '/users/:id/finish_signup' => 'users#finish_signup', via: [:get, :patch], :as => :finish_signup




  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".   

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
