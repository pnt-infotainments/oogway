# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile +=  ["slick.min.js", "slick.css",
 "slick-theme.css", "roundable/style.css", "roundable/roundabout.js",
 "scroll_deck/style.css", "scroll_deck/jquery.easing.1.3.js",
 "scroll_deck/jquery.scrolldeck.js", "scroll_deck/jquery.scrollrama.js",
 "scroll_deck/jquery.scrollTo-1.4.3.1.min.js",
 "scroll_deck/jquery.parallax-1.1.js", "explore_channels.js",
 "channels.js"] 

