# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160417063743) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "achievements", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "profile_id"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "achievements", ["profile_id"], name: "index_achievements_on_profile_id", using: :btree
  add_index "achievements", ["user_id"], name: "index_achievements_on_user_id", using: :btree

  create_table "activities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "action"
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "activities", ["trackable_id"], name: "index_activities_on_trackable_id", using: :btree
  add_index "activities", ["user_id"], name: "index_activities_on_user_id", using: :btree

  create_table "answers", force: :cascade do |t|
    t.integer  "channel_id"
    t.integer  "user_id"
    t.text     "contents"
    t.integer  "post_id"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "answers", ["channel_id"], name: "index_answers_on_channel_id", using: :btree
  add_index "answers", ["post_id"], name: "index_answers_on_post_id", using: :btree
  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree
  add_index "answers", ["user_id"], name: "index_answers_on_user_id", using: :btree

  create_table "authorizations", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "token"
    t.string   "secret"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "authorizations", ["user_id"], name: "index_authorizations_on_user_id", using: :btree

  create_table "channel_images", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "channel_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "image"
  end

  add_index "channel_images", ["channel_id"], name: "index_channel_images_on_channel_id", using: :btree

  create_table "channel_types", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "channels", force: :cascade do |t|
    t.string   "name",            null: false
    t.string   "description",     null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "slug"
    t.integer  "user_id"
    t.integer  "status_id"
    t.integer  "channel_type_id"
  end

  add_index "channels", ["channel_type_id"], name: "index_channels_on_channel_type_id", using: :btree
  add_index "channels", ["status_id"], name: "index_channels_on_status_id", using: :btree
  add_index "channels", ["user_id"], name: "index_channels_on_user_id", using: :btree

  create_table "college_details", force: :cascade do |t|
    t.string   "degree"
    t.date     "year"
    t.string   "department"
    t.integer  "college_id"
    t.integer  "profile_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "college_details", ["college_id"], name: "index_college_details_on_college_id", using: :btree
  add_index "college_details", ["profile_id"], name: "index_college_details_on_profile_id", using: :btree
  add_index "college_details", ["user_id"], name: "index_college_details_on_user_id", using: :btree

  create_table "colleges", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.string   "location"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.string   "content"
    t.integer  "user_id"
    t.boolean  "report_abuse"
    t.string   "email"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "ancestry"
  end

  add_index "comments", ["ancestry"], name: "index_comments_on_ancestry", using: :btree
  add_index "comments", ["commentable_id", "commentable_type"], name: "index_comments_on_commentable_id_and_commentable_type", using: :btree
  add_index "comments", ["user_id", "email"], name: "index_comments_on_user_id_and_email", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "content_images", force: :cascade do |t|
    t.text     "image"
    t.text     "image_url"
    t.integer  "post_content_id"
    t.integer  "post_id"
    t.integer  "channel_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "content_images", ["channel_id"], name: "index_content_images_on_channel_id", using: :btree
  add_index "content_images", ["post_content_id"], name: "index_content_images_on_post_content_id", using: :btree
  add_index "content_images", ["post_id"], name: "index_content_images_on_post_id", using: :btree

  create_table "embed_codes", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "link"
    t.float    "duration"
    t.integer  "post_id",     null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "embed_codes", ["post_id"], name: "index_embed_codes_on_post_id", using: :btree

  create_table "experiences", force: :cascade do |t|
    t.string   "company_name"
    t.string   "designation"
    t.date     "from"
    t.date     "to"
    t.integer  "profile_id"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "experiences", ["profile_id"], name: "index_experiences_on_profile_id", using: :btree
  add_index "experiences", ["user_id"], name: "index_experiences_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.integer  "recipient_id", null: false
    t.integer  "sender_id",    null: false
    t.text     "body",         null: false
    t.string   "subject",      null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "post_categories", force: :cascade do |t|
    t.string   "name",        null: false
    t.text     "description", null: false
    t.integer  "channel_id",  null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "slug"
  end

  add_index "post_categories", ["channel_id"], name: "index_post_categories_on_channel_id", using: :btree

  create_table "post_category_images", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "post_category_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.text     "image"
  end

  add_index "post_category_images", ["post_category_id"], name: "index_post_category_images_on_post_category_id", using: :btree

  create_table "post_contents", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "post_id"
    t.integer  "channel_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "post_contents", ["channel_id"], name: "index_post_contents_on_channel_id", using: :btree
  add_index "post_contents", ["post_id"], name: "index_post_contents_on_post_id", using: :btree

  create_table "post_images", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "post_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "image"
  end

  add_index "post_images", ["post_id"], name: "index_post_images_on_post_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "status",      default: false
    t.integer  "channel_id"
    t.integer  "user_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "slug"
    t.integer  "status_id"
  end

  add_index "posts", ["channel_id"], name: "index_posts_on_channel_id", using: :btree
  add_index "posts", ["status_id"], name: "index_posts_on_status_id", using: :btree
  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "facebook"
    t.string   "github"
    t.string   "linkedin"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "cover_photo"
    t.string   "avatar"
    t.string   "user_name"
    t.date     "dob"
  end

  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree
  add_index "profiles", ["user_name"], name: "index_profiles_on_user_name", using: :btree

  create_table "questions", force: :cascade do |t|
    t.integer  "channel_id"
    t.text     "contents"
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "questions", ["channel_id"], name: "index_questions_on_channel_id", using: :btree
  add_index "questions", ["post_id"], name: "index_questions_on_post_id", using: :btree
  add_index "questions", ["user_id"], name: "index_questions_on_user_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "school_details", force: :cascade do |t|
    t.string   "std"
    t.string   "name"
    t.integer  "user_id"
    t.integer  "profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "school_details", ["profile_id"], name: "index_school_details_on_profile_id", using: :btree
  add_index "school_details", ["user_id"], name: "index_school_details_on_user_id", using: :btree

  create_table "statuses", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active",      default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "subscriptions", force: :cascade do |t|
    t.string   "email"
    t.integer  "user_id"
    t.boolean  "subscribe"
    t.integer  "subscribable_id"
    t.string   "subscribable_type"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "subscriptions", ["subscribable_type", "subscribable_id"], name: "index_subscriptions_on_subscribable_type_and_subscribable_id", using: :btree
  add_index "subscriptions", ["user_id"], name: "index_subscriptions_on_user_id", using: :btree

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type"], name: "index_taggings_on_taggable_id_and_taggable_type", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "unions", force: :cascade do |t|
    t.string   "name"
    t.string   "sub_domain"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_roles", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "user_id"
    t.integer  "channel_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_roles", ["role_id"], name: "index_user_roles_on_role_id", using: :btree
  add_index "user_roles", ["user_id"], name: "index_user_roles_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "user_name"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "votes", force: :cascade do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.integer  "voter_id"
    t.string   "voter_type"
    t.boolean  "vote_flag"
    t.string   "vote_scope"
    t.integer  "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

  add_foreign_key "achievements", "profiles"
  add_foreign_key "achievements", "users"
  add_foreign_key "activities", "users"
  add_foreign_key "answers", "channels"
  add_foreign_key "answers", "posts"
  add_foreign_key "answers", "questions"
  add_foreign_key "answers", "users"
  add_foreign_key "authorizations", "users"
  add_foreign_key "channel_images", "channels"
  add_foreign_key "channels", "channel_types"
  add_foreign_key "channels", "statuses"
  add_foreign_key "channels", "users"
  add_foreign_key "college_details", "colleges"
  add_foreign_key "college_details", "profiles"
  add_foreign_key "college_details", "users"
  add_foreign_key "comments", "users"
  add_foreign_key "content_images", "channels"
  add_foreign_key "content_images", "post_contents"
  add_foreign_key "content_images", "posts"
  add_foreign_key "embed_codes", "posts"
  add_foreign_key "experiences", "profiles"
  add_foreign_key "experiences", "users"
  add_foreign_key "messages", "users", column: "recipient_id"
  add_foreign_key "messages", "users", column: "sender_id"
  add_foreign_key "post_categories", "channels"
  add_foreign_key "post_category_images", "post_categories"
  add_foreign_key "post_contents", "channels"
  add_foreign_key "post_contents", "posts"
  add_foreign_key "post_images", "posts"
  add_foreign_key "posts", "channels"
  add_foreign_key "posts", "statuses"
  add_foreign_key "posts", "users"
  add_foreign_key "profiles", "users"
  add_foreign_key "questions", "channels"
  add_foreign_key "questions", "posts"
  add_foreign_key "questions", "users"
  add_foreign_key "school_details", "profiles"
  add_foreign_key "school_details", "users"
  add_foreign_key "subscriptions", "users"
  add_foreign_key "taggings", "tags"
  add_foreign_key "user_roles", "roles"
  add_foreign_key "user_roles", "users"
end
