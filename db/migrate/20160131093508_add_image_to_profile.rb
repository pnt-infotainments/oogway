class AddImageToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :cover_photo, :string
    add_column :profiles, :avatar, :string
  end
end
