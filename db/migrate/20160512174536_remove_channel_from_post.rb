class RemoveChannelFromPost < ActiveRecord::Migration
  def change
    remove_reference :posts, :channel, index: true, foreign_key: true
  end
end
