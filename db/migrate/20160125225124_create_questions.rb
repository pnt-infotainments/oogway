class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :channel, index: true, foreign_key: true
      t.text :contents
      t.references :user, index: true, foreign_key: true
      t.references :post, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
