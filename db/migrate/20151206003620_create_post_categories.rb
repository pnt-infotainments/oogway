class CreatePostCategories < ActiveRecord::Migration
  def change
    create_table :post_categories do |t|
      t.string :name, null: false
      t.text :description, null: false
      t.references :channel, index: true, foreign_key: true, null: false
      t.timestamps null: false
    end
  end
end
