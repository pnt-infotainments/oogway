class CreateUserRoles < ActiveRecord::Migration
  def change
    create_table :user_roles do |t|
      t.references :role, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.integer :channel_id

      t.timestamps null: false
    end
  end
end
