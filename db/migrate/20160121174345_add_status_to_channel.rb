class AddStatusToChannel < ActiveRecord::Migration
  def change
    add_reference :channels, :status, index: true, foreign_key: true
  end
end
