class AddImageToOwnModel < ActiveRecord::Migration
  def change
  	add_column :channels, :image, :text
    add_column :institutions, :image, :text
  end
end
