class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :email
      t.references :user, index: true, foreign_key: true
      t.boolean :subscribe
      t.references :subscribable, index: true, polymorphic: true

      t.timestamps null: false
    end
  end
end
