class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.references :user, index: true, foreign_key: true
      t.references :attachable, index: true, polymorphic: true
      t.text :link

      t.timestamps null: false
    end
    add_index :attachments, [:attachable_id, :attachable_type]
  end
end
