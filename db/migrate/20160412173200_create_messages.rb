class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :recipient_id, null: false
      t.integer :sender_id, null: false
      t.text :body, null: false
      t.string :subject, null: false
      t.timestamps null: false
    end
    	add_foreign_key :messages, :users, column: :recipient_id
    	add_foreign_key :messages, :users, column: :sender_id
  end
end
