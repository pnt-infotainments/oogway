class AddCollegeIdToProfile < ActiveRecord::Migration
  def change
  	add_reference :profiles, :college, index: true, foreign_key: true
  end
end
