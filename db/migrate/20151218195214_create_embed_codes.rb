class CreateEmbedCodes < ActiveRecord::Migration
  def change
    create_table :embed_codes do |t|
      t.string :name
      t.text :description
      t.string :link
      t.float :duration
      t.references :post, index: true, foreign_key: true, null: false
      t.timestamps null: false
    end
  end
end
