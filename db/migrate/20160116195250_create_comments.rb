class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :content
      t.references :user, index: true, foreign_key: true
      t.boolean :report_abuse
      t.string :email
      t.references :commentable, polymorphic: true

      t.timestamps null: false

    end
    add_index :comments, [:commentable_id, :commentable_type]
    add_index :comments, [:user_id, :email]
  end
end
