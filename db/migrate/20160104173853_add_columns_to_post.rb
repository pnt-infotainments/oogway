class AddColumnsToPost < ActiveRecord::Migration
  def change
    add_reference :posts, :status, index: true, foreign_key: true
  end
end
