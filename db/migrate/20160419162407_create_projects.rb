class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.references :user, index: true, foreign_key: true
      t.references :profile, index: true, foreign_key: true
      t.string :title
      t.text :description
      t.date :duration
      t.string :role
      t.integer :team_size
      t.timestamps null: false
    end
  end
end
