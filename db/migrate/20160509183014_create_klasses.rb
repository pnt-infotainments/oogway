class CreateKlasses < ActiveRecord::Migration
  def change
    create_table :klasses do |t|
      t.string :title
      t.text :description
      t.string :code
      t.integer :capacity
      t.string :avatar
      t.references :user, index: true, foreign_key: true
      t.string :slug
      t.timestamps null: false
    end
  end
end
