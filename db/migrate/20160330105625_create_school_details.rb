class CreateSchoolDetails < ActiveRecord::Migration
  def change
    create_table :school_details do |t|
      t.string :std
      t.string :name
      t.references :user, index: true, foreign_key: true
      t.references :profile, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
