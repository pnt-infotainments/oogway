class CreatePostContents < ActiveRecord::Migration
  def change
    create_table :post_contents do |t|
      t.string :name
      t.text :description
      t.references :post, index: true, foreign_key: true
      t.references :channel, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
