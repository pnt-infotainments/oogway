class CreateTaggings < ActiveRecord::Migration
  def change
    create_table :taggings do |t|
      t.belongs_to :tag, index: true, foreign_key: true
      t.references :taggable, polymorphic: true
      t.timestamps null: false
    end
    add_index :taggings, [:taggable_id, :taggable_type]
  end
end
