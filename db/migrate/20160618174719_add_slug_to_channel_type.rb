class AddSlugToChannelType < ActiveRecord::Migration
  def change
    add_column :channel_types, :slug, :string
  end
end
