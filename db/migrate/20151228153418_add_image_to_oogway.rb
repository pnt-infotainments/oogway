class AddImageToOogway < ActiveRecord::Migration
  def change
    add_column :channel_images, :image, :text
    add_column :post_category_images, :image, :text
    add_column :post_images, :image, :text
  end
end
