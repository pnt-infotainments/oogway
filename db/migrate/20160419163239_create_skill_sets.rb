class CreateSkillSets < ActiveRecord::Migration
  def change
    create_table :skill_sets do |t|
      t.references :skill, index: true, foreign_key: true
      t.references :skillable, polymorphic: true
      t.timestamps null: false
    end
    add_index :skill_sets, [:skillable_id, :skillable_type]
  end
end
