class AddUserNameToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :user_name, :string, unique: true
    add_index :profiles, :user_name
  end
end
