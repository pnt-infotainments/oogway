class AddChannelTypeToChannel < ActiveRecord::Migration
  def change
    add_reference :channels, :channel_type, index: true, foreign_key: true
  end
end
