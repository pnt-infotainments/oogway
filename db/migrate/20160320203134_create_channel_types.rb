class CreateChannelTypes < ActiveRecord::Migration
  def change
    create_table :channel_types do |t|
      t.string :title
      t.text :description
      t.string :image

      t.timestamps null: false
    end
  end
end
