class CreateContentImages < ActiveRecord::Migration
  def change
    create_table :content_images do |t|
      t.text :image
      t.text :image_url
      t.references :post_content, index: true, foreign_key: true
      t.references :post, index: true, foreign_key: true
      t.references :channel, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
