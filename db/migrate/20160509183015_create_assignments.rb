class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.references :klass, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.references :assignable, index: true, polymorphic: true
      t.string :title
      t.text :description
      t.date :due_date

      t.timestamps null: false
    end
    add_index :assignments, [:assignable_id, :assignable_type]
  end
end
