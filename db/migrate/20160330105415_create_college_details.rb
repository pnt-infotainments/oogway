class CreateCollegeDetails < ActiveRecord::Migration
  def change
    create_table :college_details do |t|
      t.string :degree
      t.date :year
      t.string :department
      t.references :college, index: true, foreign_key: true
      t.references :profile, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
