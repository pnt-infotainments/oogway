class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.references :user, index: true, foreign_key: true
      t.boolean :status, default: false
      t.date :end_date
      t.boolean :block, default: false
      t.references :klass, index: true, foreign_key: true
      t.string :remarks

      t.timestamps null: false
    end
  end
end
