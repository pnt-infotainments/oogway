class CreateChannelImages < ActiveRecord::Migration
  def change
    create_table :channel_images do |t|
      t.string :name
      t.string :description
      t.references :channel, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
