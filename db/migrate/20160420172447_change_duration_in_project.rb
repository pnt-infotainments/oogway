class ChangeDurationInProject < ActiveRecord::Migration
  def change
  	add_column :projects, :from, :date
  	add_column :projects, :to, :date
  	add_column :projects, :type, :string
  	remove_column :projects, :duration
  end
end
