class AddInstitutionToKlasses < ActiveRecord::Migration
  def change
    add_reference :klasses, :institution, index: true, foreign_key: true
  end
end
