class CreateStatuses < ActiveRecord::Migration
  def change
    create_table :statuses do |t|
      t.string :name
      t.text :description
      t.boolean :active, default: false
      t.timestamps null: false
    end
  end
end
