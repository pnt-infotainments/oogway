class RemoveColumnsFromProfile < ActiveRecord::Migration
  def change
  	remove_column :profiles, :job_details
  	remove_column :profiles, :college_id
  	remove_column :profiles, :education
  end
end
