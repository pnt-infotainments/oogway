class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references :user, index: true, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.string :facebook
      t.string :github
      t.string :linkedin
      t.string :education
      t.string :job_details

      t.timestamps null: false
    end
  end
end
