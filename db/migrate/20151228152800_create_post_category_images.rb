class CreatePostCategoryImages < ActiveRecord::Migration
  def change
    create_table :post_category_images do |t|
      t.string :name
      t.string :description
      t.references :post_category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
